
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>CarWorld </title>
<!-- Stylesheets -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="plugins/revolution/css/settings.css" rel="stylesheet" type="text/css"><!-- REVOLUTION SETTINGS STYLES -->
<link href="plugins/revolution/css/layers.css" rel="stylesheet" type="text/css"><!-- REVOLUTION LAYERS STYLES -->
<link href="plugins/revolution/css/navigation.css" rel="stylesheet" type="text/css"><!-- REVOLUTION NAVIGATION STYLES -->
<link href="css/style.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">

<!--Favicon-->
<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
<link rel="icon" href="images/favicon.png" type="image/x-icon">
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>

<body>
<div class="page-wrapper">
 	
    <?php include("include/header.php") ?>
    
    <!--Main Slider-->
    <section class="main-slider">
    	
        <div class="rev_slider_wrapper fullwidthbanner-container"  id="rev_slider_one_wrapper" data-source="gallery">
            <div class="rev_slider fullwidthabanner" id="rev_slider_one" data-version="5.4.1">
                <ul>
                	
                    <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1687" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="images/main-slider/image-1.jpg" data-title="Slide Title" data-transition="parallaxvertical">
                    <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="images/main-slider/image-1.jpg"> 
                    
                    <div class="tp-caption" 
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingtop="[0,0,0,0]"
                    data-responsive_offset="on"
                    data-type="text"
                    data-height="none"
                    data-width="['600','600','500','420']"
                    data-whitespace="normal"
                    data-hoffset="['15','15','15','15']"
                    data-voffset="['-20','-20','-20','-20']"
                    data-x="['right','right','right','right']"
                    data-y="['middle','middle','middle','middle']"
                    data-textalign="['top','top','top','top']"
                    data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                    	<div class="slider-content">
                        	<h2>Find your dream car <br> with carworld!.</h2>
                            <div class="text">It is a long established fact that a reader will distracted by the readable content of a page when looking.</div>
                            <a href="#" class="theme-btn btn-style-one">Request a Demo</a>
                        </div>
                    </div>
                    
                    </li>
                    
                    <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1688" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="images/main-slider/image-2.jpg" data-title="Slide Title" data-transition="parallaxvertical">
                    <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="images/main-slider/image-2.jpg"> 
                    
                    <div class="tp-caption" 
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingtop="[0,0,0,0]"
                    data-responsive_offset="on"
                    data-type="text"
                    data-height="none"
                    data-width="['600','600','500','420']"
                    data-whitespace="normal"
                    data-hoffset="['15','15','15','15']"
                    data-voffset="['-20','-20','-20','-20']"
                    data-x="['right','right','right','right']"
                    data-y="['middle','middle','middle','middle']"
                    data-textalign="['top','top','top','top']"
                    data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                    	<div class="slider-content">
                        	<h2>Find your dream car <br> with carworld!.</h2>
                            <div class="text">It is a long established fact that a reader will distracted by the readable content of a page when looking.</div>
                            <a href="#" class="theme-btn btn-style-one">Request a Demo</a>
                        </div>
                    </div>
                    
                    </li>
                    
                </ul>
                
            </div>
        </div>
    </section>
    <!--End Main Slider-->
    
    <!--Car Search Form-->
    <section class="car-search-form">
    	<div class="auto-container">
        	<div class="inner-section">
            	
                <!--Product Info Tabs-->
                <div class="car-search-tab">
                
                    <!--Tabs Box-->
                    <div class="prod-tabs tabs-box">
                    
                        <!--Tab Btns-->
                        <ul class="tab-btns tab-buttons clearfix">
                            <li data-tab="#new-car" class="tab-btn active-btn">New Cars</li>
							<li data-tab="#used-car" class="tab-btn ">Used Cars</li>
                            
                            <li data-tab="#value-my-car" class="tab-btn">Value My Car</li>
                        </ul>
                        
                        <!--Tabs Container-->
                        <div class="tabs-content">
                            
                            <!--Tab / Active Tab-->
                            <div class="tab " id="used-car">
                                <div class="content">
                                    <!-- Car Search Form -->
                                    <div class="car-search-form">
                                        <form method="post" action="news.html">
                                            <div class="row clearfix">
                                            	<div class="column col-lg-7 col-md-12 col-sm-12 col-xs-12">
                                                	<div class="row clearfix">
                                                    	
                                                        <!--Form Group-->
                                                        <div class="form-group col-md-4 col-sm-4 col-xs-12">
                                                            <select class="custom-select-box">
                                                                <option>All Makes</option>
                                                                <option>Makes One</option>
                                                                <option>Makes Two</option>
                                                                <option>Makes Three</option>
                                                                <option>Makes Four</option>
                                                            </select>
                                                        </div>
                                                        
                                                        <!--Form Group-->
                                                        <div class="form-group col-md-4 col-sm-4 col-xs-12">
                                                            <select class="custom-select-box">
                                                                <option>All Model</option>
                                                                <option>Model One</option>
                                                                <option>Model Two</option>
                                                                <option>Model Three</option>
                                                                <option>Model Four</option>
                                                            </select>
                                                        </div>
                                                        
                                                        <!--Form Group-->
                                                        <div class="form-group col-md-4 col-sm-4 col-xs-12">
                                                            <select class="custom-select-box">
                                                                <option>Max.Price</option>
                                                                <option>Price One</option>
                                                                <option>Price Two</option>
                                                                <option>Price Three</option>
                                                                <option>Price Four</option>
                                                            </select>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                <div class="column col-lg-5 col-md-12 col-sm-12 col-xs-12">
                                                	<div class="row clearfix">
                                                        <!--Form Group-->
                                                        <div class="form-group col-md-4 col-sm-4 col-xs-12">
                                                            <select class="custom-select-box">
                                                                <option>Mileage</option>
                                                                <option>Mileage One</option>
                                                                <option>Mileage Two</option>
                                                                <option>Mileage Three</option>
                                                                <option>Mileage Four</option>
                                                            </select>
                                                        </div>
                                                        
                                                        <!--Form Group-->
                                                        <div class="form-group col-md-4 col-sm-4 col-xs-12">
                                                            <select class="custom-select-box">
                                                                <option>Newyork City</option>
                                                                <option>Chicago One</option>
                                                                <option>California Two</option>
                                                            </select>
                                                        </div>
                                                        
                                                        <div class="form-group col-md-4 col-sm-4 col-xs-12">
                                                            <button class="theme-btn search-btn" type="submit" name="submit-form">Search</button>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                
                                            </div>
                                        </form>
                
                                    </div>
                                </div>
                            </div>
                            
                            <!--Tab-->
                            <div class="tab active-tab" id="new-car">
                                <div class="content">
                                    <!-- Car Search Form -->
                                    <div class="car-search-form">
                                        <form method="post" action="news.html">
                                            <div class="row clearfix">
                                            	<div class="column col-lg-7 col-md-12 col-sm-12 col-xs-12">
                                                	<div class="row clearfix">
                                                    	
                                                        <!--Form Group-->
                                                        <div class="form-group col-md-4 col-sm-4 col-xs-12">
                                                            <select class="custom-select-box">
                                                                <option>All Makes</option>
                                                                <option>Makes One</option>
                                                                <option>Makes Two</option>
                                                                <option>Makes Three</option>
                                                                <option>Makes Four</option>
                                                            </select>
                                                        </div>
                                                        
                                                        <!--Form Group-->
                                                        <div class="form-group col-md-4 col-sm-4 col-xs-12">
                                                            <select class="custom-select-box">
                                                                <option>All Model</option>
                                                                <option>Model One</option>
                                                                <option>Model Two</option>
                                                                <option>Model Three</option>
                                                                <option>Model Four</option>
                                                            </select>
                                                        </div>
                                                        
                                                        <!--Form Group-->
                                                        <div class="form-group col-md-4 col-sm-4 col-xs-12">
                                                            <select class="custom-select-box">
                                                                <option>Max.Price</option>
                                                                <option>Price One</option>
                                                                <option>Price Two</option>
                                                                <option>Price Three</option>
                                                                <option>Price Four</option>
                                                            </select>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                <div class="column col-lg-5 col-md-12 col-sm-12 col-xs-12">
                                                	<div class="row clearfix">
                                                        <!--Form Group-->
                                                        <div class="form-group col-md-4 col-sm-4 col-xs-12">
                                                            <select class="custom-select-box">
                                                                <option>Mileage</option>
                                                                <option>Mileage One</option>
                                                                <option>Mileage Two</option>
                                                                <option>Mileage Three</option>
                                                                <option>Mileage Four</option>
                                                            </select>
                                                        </div>
                                                        
                                                        <!--Form Group-->
                                                        <div class="form-group col-md-4 col-sm-4 col-xs-12">
                                                            <select class="custom-select-box">
                                                                <option>Newyork City</option>
                                                                <option>Chicago One</option>
                                                                <option>California Two</option>
                                                            </select>
                                                        </div>
                                                        
                                                        <div class="form-group col-md-4 col-sm-4 col-xs-12">
                                                            <button class="theme-btn search-btn" type="submit" name="submit-form">Search</button>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                
                                            </div>
                                        </form>
                
                                    </div>
                                </div>
                            </div>
							
							<!--Tab-->
                            <div class="tab" id="value-my-car">
                                <div class="content">
                                    <!-- Car Search Form -->
                                    <div class="car-search-form">
                                        <form method="post" action="news.html">
                                            <div class="row clearfix">
                                            	<div class="column col-lg-7 col-md-12 col-sm-12 col-xs-12">
                                                	<div class="row clearfix">
                                                    	
                                                        <!--Form Group-->
                                                        <div class="form-group col-md-4 col-sm-4 col-xs-12">
                                                            <select class="custom-select-box">
                                                                <option>All Makes</option>
                                                                <option>Makes One</option>
                                                                <option>Makes Two</option>
                                                                <option>Makes Three</option>
                                                                <option>Makes Four</option>
                                                            </select>
                                                        </div>
                                                        
                                                        <!--Form Group-->
                                                        <div class="form-group col-md-4 col-sm-4 col-xs-12">
                                                            <select class="custom-select-box">
                                                                <option>All Model</option>
                                                                <option>Model One</option>
                                                                <option>Model Two</option>
                                                                <option>Model Three</option>
                                                                <option>Model Four</option>
                                                            </select>
                                                        </div>
                                                        
                                                        <!--Form Group-->
                                                        <div class="form-group col-md-4 col-sm-4 col-xs-12">
                                                            <select class="custom-select-box">
                                                                <option>Max.Price</option>
                                                                <option>Price One</option>
                                                                <option>Price Two</option>
                                                                <option>Price Three</option>
                                                                <option>Price Four</option>
                                                            </select>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                <div class="column col-lg-5 col-md-12 col-sm-12 col-xs-12">
                                                	<div class="row clearfix">
                                                        <!--Form Group-->
                                                        <div class="form-group col-md-4 col-sm-4 col-xs-12">
                                                            <select class="custom-select-box">
                                                                <option>Mileage</option>
                                                                <option>Mileage One</option>
                                                                <option>Mileage Two</option>
                                                                <option>Mileage Three</option>
                                                                <option>Mileage Four</option>
                                                            </select>
                                                        </div>
                                                        
                                                        <!--Form Group-->
                                                        <div class="form-group col-md-4 col-sm-4 col-xs-12">
                                                            <select class="custom-select-box">
                                                                <option>Newyork City</option>
                                                                <option>Chicago One</option>
                                                                <option>California Two</option>
                                                            </select>
                                                        </div>
                                                        
                                                        <div class="form-group col-md-4 col-sm-4 col-xs-12">
                                                            <button class="theme-btn search-btn" type="submit" name="submit-form">Search</button>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                
                                            </div>
                                        </form>
                
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
                <!--End Product Info Tabs-->
                
            </div>
        </div>
    </section>
    <!--End Car Search Form-->
    
    <!--Popular Cars Section-->
    <section class="popular-cars-section">
    	<div class="auto-container">
        	<!--Sec Title-->
    		<div class="sec-title">
            	<h2>Popular New Cars</h2>
            </div>
    		<!--End Sec Title-->
            <div class="row clearfix">
            	<?php 
					include("admin/include/config.php");
					
					$query ="SELECT * FROM addcar";
					
					$run_query= mysqli_query($conn,$query);
					
					
					while($row = $run_query->fetch_assoc()){
	
				
				?>
				
				<!--Car Block-->
                <div class="car-block col-lg-3 col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image">
                        	<a href="car.php?id=<?php echo $row['id']; ?>"><img src="images/resource/car-1.png" alt="" /></a>
                            <div class="price">$<?php echo $row["price"]; ?></div>
                        </div>
                        <h3><a href="car.php?id=<?php echo $row['id']; ?>"><?php echo $row["name"]; ?></a></h3>
                        <div class="lower-box">
                        	<ul class="car-info">
                            	<li><span class="fa fa-road icon"></span><?php echo $row["mile"]; ?></li>
                                <li><span class="icon fa fa-car"></span><?php echo $row["fuel"]; ?></li>
                                <li><span class="icon fa fa-clock-o"></span><?php echo $row["model"]; ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <?php } ?>	
                
            </div>
        </div>
    </section>
    <!--End Popular Cars Section-->
    
    <!--Counter Section-->
    <section class="counter-section" style="background-image:url(images/background/1.jpg);">
    	<div class="auto-container">
        		
            <div class="fact-counter">
                <div class="row clearfix">
                
                    <!--Column-->
                    <div class="column counter-column col-md-3 col-sm-6 col-xs-12">
                        <div class="inner">
                        	<div class="content">
                                <div class="count-outer count-box">
                                	<div class="icon-box"><span class="icon flaticon-transport-1"></span></div>
                                    <span class="count-text" data-speed="2500" data-stop="4350">0</span>
                                </div>
                                <h4 class="counter-title">Total Cars</h4>
                            </div>
                        </div>
                    </div>
					<!--Column-->
                    <div class="column counter-column col-md-3 col-sm-6 col-xs-12">
                        <div class="inner">
                        	<div class="content">
                                <div class="count-outer count-box">
                                	<div class="icon-box"><span class="icon flaticon-transport-1"></span></div>
                                    <span class="count-text" data-speed="3000" data-stop="3460">0</span>
                                </div>
                                <h4 class="counter-title">Sold Cars</h4>
                            </div>
                        </div>
                    </div>
                    <!--Column-->
                    <div class="column counter-column col-md-3 col-sm-6 col-xs-12">
                        <div class="inner">
                        	<div class="content">
                                <div class="count-outer count-box">
                                	<div class="icon-box"><span class="icon flaticon-good-mood-emoticon"></span></div>
                                    <span class="count-text" data-speed="3000" data-stop="99">0</span>%
                                </div>
                                <h4 class="counter-title">Satisfied Customers</h4>
                            </div>
                        </div>
                    </div>
            
                    <!--Column-->
                    <div class="column counter-column col-md-3 col-sm-6 col-xs-12">
                        <div class="inner">
                        	<div class="content">
                                <div class="count-outer count-box">
                                	<div class="icon-box"><span class="icon flaticon-black"></span></div>
                                    <span class="count-text" data-speed="3000" data-stop="295">0</span>+
                                </div>
                                <h4 class="counter-title">Experts Reviews</h4>
                            </div>
                        </div>
                    </div>
                    
                    
            
                </div>
            </div>
            
        </div>
    </section>
    <!--End Counter Section-->
    
    <!--Popular Cars Section Two-->
    <section class="popular-cars-section-two">
    	<div class="auto-container">
        	<!--Sec Title-->
            <div class="sec-title centered">
            	<h2>Popular Used Cars</h2>
            </div>
            
            <!--Popular Used Tabs-->
            <div class="popular-used-car">
            
                <!--Tabs Box-->
                <div class="prod-tabs tabs-box">
                	<div class="text-center">
                        <!--Tab Btns-->
                        <ul class="tab-btns tab-buttons clearfix">
                            <li data-tab="#prod-all" class="tab-btn"><span class="text">View All</span></li>
                            <li data-tab="#prod-city" class="tab-btn active-btn"><span class="text">By City</span><span class="icon flaticon-planet-earth"></span></li>
                            <li data-tab="#prod-brand" class="tab-btn"><span class="text">By Brand</span><span class="icon flaticon-check-2"></span></li>
                            <li data-tab="#prod-price" class="tab-btn"><span class="text">By Price</span><span class="icon fa fa-credit-card"></span></li>
                            <li data-tab="#prod-body" class="tab-btn"><span class="text">By Body</span><span class="icon flaticon-car"></span></li>
                        </ul>
                    </div>
                    
                    <!--Tabs Container-->
                    <div class="tabs-content">
                        
                        <!--Tab-->
                        <div class="tab" id="prod-all">
                            <div class="content">
                                <div class="row clearfix">
                                
                                	<!--City Block-->
                                    <div class="city-block col-md-3 col-sm-4 col-xs-12">
                                    	<div class="inner-box">
                                        	<div class="icon-box">
                                            	<span class="icon flaticon-statue-of-liberty"></span>
                                            </div>
                                            <h3><a href="shop.html">Newyork City</a></h3>
                                        </div>
                                    </div>
                                    
                                   
                                    
                                </div>
                                
                                <div class="row clearfix">
                                
                                	<!--Brand Block-->
                                    <div class="brand-block col-md-3 col-sm-4 col-xs-12">
                                    	<div class="inner-box">
                                        	<figure class="image"><a href="#"><img src="images/clients/27.png" alt=""></a></figure>
                                        </div>
                                    </div>
                                    
                                   
                                </div>
                                
                                <div class="row clearfix">
                                
                                	<!--Price Block-->
                                    <div class="price-block col-md-3 col-sm-4 col-xs-12">
                                    	<div class="inner-box">
                                        	<a href="#" class="text">Under 1 Lakh</a>
                                        </div>
                                    </div>
                                    
                                    
                                </div>
                                
                                <div class="row clearfix">
                                
                                	<!--Body Block-->
                                    <div class="body-block col-md-3 col-sm-4 col-xs-12">
                                    	<div class="inner-box">
                                        	<a href="#" class="link-box"><div class="icon-box"><img src="images/icons/car-icons/1.png" alt=""></div><div class="text">Convertible (28)</div></a>
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    
                                </div>
                                
                            </div>
                        </div>
                        
                        <!--Tab / Active Tab-->
                        <div class="tab active-tab" id="prod-city">
                            <div class="content">
                                <div class="row clearfix">
                                
                                	<!--City Block-->
                                    <div class="city-block col-md-3 col-sm-4 col-xs-12">
                                    	<div class="inner-box">
                                        	<div class="icon-box">
                                            	<span class="icon flaticon-statue-of-liberty"></span>
                                            </div>
                                            <h3><a href="shop.html">Newyork City</a></h3>
                                        </div>
                                    </div>
                                    
                                    
                                </div>
                                
                            </div>
                        </div>
                        
                        <!--Tab-->
                        <div class="tab" id="prod-brand">
                            <div class="content">
                                
                                <div class="row clearfix">
                                
                                	<!--Brand Block-->
                                    <div class="brand-block col-md-3 col-sm-4 col-xs-12">
                                    	<div class="inner-box">
                                        	<figure class="image"><a href="#"><img src="images/clients/27.png" alt=""></a></figure>
                                        </div>
                                    </div>
                                    
                                    
                                    
                                </div>
                                
                            </div>
                        </div>
                        
                        <!--Tab-->
                        <div class="tab" id="prod-price">
                            <div class="content">
                                <div class="row clearfix">
                                
                                	<!--Price Block-->
                                    <div class="price-block col-md-3 col-sm-4 col-xs-12">
                                    	<div class="inner-box">
                                        	<a href="#" class="text">Under 1 Lakh</a>
                                        </div>
                                    </div>
                                    
                                    
                                </div>
                            </div>
                        </div>
                        
                        <!--Tab-->
                        <div class="tab" id="prod-body">
                            <div class="content">
                                <div class="row clearfix">
                                
                                	<!--Body Block-->
                                    <div class="body-block col-md-3 col-sm-4 col-xs-12">
                                    	<div class="inner-box">
                                        	<a href="#" class="link-box"><div class="icon-box"><img src="images/icons/car-icons/1.png" alt=""></div><div class="text">Convertible (28)</div></a>
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                
            </div>
            <!--End Product Info Tabs-->
            
        </div>
    </section>
    <!--End Popular Cars Section Two-->
    
    <!--Services Section-->
    <section class="services-section" style="background-image:url(images/background/2.jpg);">
    	<div class="auto-container">
        	<div class="row clearfix">
            	<!--Services Block-->
                <div class="services-block col-md-6 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="upper-box" style="background-image:url(images/resource/service-1.jpg);">
                        	<div class="upper-content">
                            	<div class="icon-box">
                                	<span class="icon flaticon-buy-a-car"></span>
                                </div>
                                <h2><a href="inventory-single.html">Do You Want to Sell a Car?</a></h2>
                                <div class="text">If you want to sell your beautiful car quickly, We will help you. You got a100% Reasonable price for your car.</div>
                            </div>
                        </div>
                        <div class="lower-content">
                            <ul>
                                <li><span class="icon flaticon-users"></span>Largest Number of Genuine <br> Buyers.</li>
                                <li><span class="icon flaticon-speedometer"></span>Speedy and Easy Car Selling <br> Process.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <!--Services Block-->
                <div class="services-block col-md-6 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="upper-box" style="background-image:url(images/resource/service-1.jpg);">
                        	<div class="upper-content">
                            	<div class="icon-box">
                                	<span class="icon flaticon-transport"></span>
                                </div>
                                <h2><a href="inventory-single.html">Car Finance & Insurance</a></h2>
                                <div class="text">Explain to you how all this mistaken idea of denouncing pleasure and praising pain was born complete.</div>
                            </div>
                        </div>
                        <div class="lower-content">
                            <ul>
                                <li><span class="icon flaticon-accounting"></span>Apply Now for Personalised, <br> Finance Quote.</li>
                                <li><span class="icon flaticon-car-washing"></span>Save Up to $140 On Car <br> Insurance.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!--End Services Section-->
    <!--Choose Section-->
    <section class="choose-section" style="background-image:url(images/background/3.jpg);">
    	<div class="auto-container">
        	<!--Sec Title-->
            <div class="sec-title style-two">
            	<h2>Why Choose Us</h2>
            </div>
            <div class="row clearfix">
            
            	<!--Services Block-->
                <div class="services-block-two col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="icon-box"><span class="icon flaticon-car-washing"></span></div>
                    	<h3><a href="services.html">Auto Loan Facility</a></h3>
                        <div class="sub-title">Easy Finance</div>
                        <div class="text">How all this mistakens idea off ut denouncing pleasures and praisings ut pain.</div>
                        <ul>
                        	<li>Professional Finance</li>
                            <li>Affordable EMI</li>
                            <li>Less Interest Rate</li>
                        </ul>
                    </div>
                </div>
                
                <!--Services Block-->
                <div class="services-block-two col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="icon-box"><span class="icon flaticon-contract"></span></div>
                    	<h3><a href="services.html">Free Documentation</a></h3>
                        <div class="sub-title">No Hidden Charges</div>
                        <div class="text">Denouncing pleasures and ut praisings pains was born work will gives you.</div>
                        <ul>
                        	<li>Quick Documentation</li>
                            <li>Very Confidential</li>
                            <li>On Time Processing</li>
                        </ul>
                    </div>
                </div>
                
                <!--Services Block-->
                <div class="services-block-two col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="icon-box"><span class="icon flaticon-telemarketer"></span></div>
                    	<h3><a href="services.html">Customer Support</a></h3>
                        <div class="sub-title">24/7 Online Support</div>
                        <div class="text">Idea of denouncing pleasure ut and praisings pain born and system and expound.</div>
                        <ul>
                        	<li>Experienced Team</li>
                            <li>Humble Talk</li>
                            <li>Quick Response</li>
                        </ul>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!--End Choose Section-->
    
    
    <!--Offer Section-->
    <section class="offer-section">
    	<div class="auto-container">
        	<!--Sec Title-->
            <div class="sec-title light centered">
            	<h2>Special Offers</h2>
            </div>
            <div class="three-item-carousel owl-carousel owl-theme">
            
            	<!--Offer Block-->
                <div class="offer-block">
                	<div class="inner-box">
                    	<div class="image">
                        	<a href="#"><img src="images/resource/car-16.png" alt="" /></a>
                            <div class="price">1.5 <span class="percent">% <br> APR</span></div>
                        </div>
                        <h3><a href="#">BMW F12 6 Series Midsized <br> Convertible</a></h3>
                        <div class="lower-box">
                        	<div class="plus-box">
                            	<span class="icon fa fa-plus"></span>
                                <ul class="tooltip-data">
                                	<li>5 Years Warranty</li>
                                    <li>Free Registration</li>
                                    <li>Free 2 Years Services</li>
                                </ul>
                            </div>
                        	<div class="lower-price">$65000</div>
                            <ul>
                            	<li><span class="icon fa fa-road"></span>26000</li>
                                <li><span class="icon fa fa-car"></span>Diesel</li>
                                <li><span class="icon fa fa-clock-o"></span>2014</li>
                                <li><span class="icon fa fa-gear"></span>Automatic</li>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <!--Offer Block-->
                <div class="offer-block">
                	<div class="inner-box">
                    	<div class="image">
                        	<a href="#"><img src="images/resource/car-17.png" alt="" /></a>
                        </div>
                        <h3><a href="#">Volkswagen Touareg Personal Luxuary Car</a></h3>
                        <div class="lower-box">
                        	<div class="plus-box">
                            	<span class="icon fa fa-plus"></span>
                                <ul class="tooltip-data">
                                	<li>5 Years Warranty</li>
                                    <li>Free Registration</li>
                                    <li>Free 2 Years Services</li>
                                </ul>
                            </div>
                        	<div class="lower-price">$78900</div>
                            <ul>
                            	<li><span class="icon fa fa-road"></span>41500</li>
                                <li><span class="icon fa fa-car"></span>petrol</li>
                                <li><span class="icon fa fa-clock-o"></span>2010</li>
                                <li><span class="icon fa fa-gear"></span>Manual</li>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <!--Offer Block-->
                <div class="offer-block">
                	<div class="inner-box">
                    	<div class="image">
                        	<a href="#"><img src="images/resource/car-18.png" alt="" /></a>
                        </div>
                        <h3><a href="#">Renault Dauphine Campact car Sedan</a></h3>
                        <div class="lower-box">
                        	<div class="plus-box">
                            	<span class="icon fa fa-plus"></span>
                                <ul class="tooltip-data">
                                	<li>5 Years Warranty</li>
                                    <li>Free Registration</li>
                                    <li>Free 2 Years Services</li>
                                </ul>
                            </div>
                        	<div class="lower-price">$56000</div>
                            <ul>
                            	<li><span class="icon fa fa-road"></span>15000</li>
                                <li><span class="icon fa fa-car"></span>Deisel</li>
                                <li><span class="icon fa fa-clock-o"></span>2015</li>
                                <li><span class="icon fa fa-gear"></span>Automatic</li>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <!--Offer Block-->
                <div class="offer-block">
                	<div class="inner-box">
                    	<div class="image">
                        	<a href="#"><img src="images/resource/car-16.png" alt="" /></a>
                            <div class="price">1.5 <span class="percent">% <br> APR</span></div>
                        </div>
                        <h3><a href="#">BMW F12 6 Series Midsized <br> Convertible</a></h3>
                        <div class="lower-box">
                        	<div class="plus-box">
                            	<span class="icon fa fa-plus"></span>
                                <ul class="tooltip-data">
                                	<li>5 Years Warranty</li>
                                    <li>Free Registration</li>
                                    <li>Free 2 Years Services</li>
                                </ul>
                            </div>
                        	<div class="lower-price">$65000</div>
                            <ul>
                            	<li><span class="icon fa fa-road"></span>26000</li>
                                <li><span class="icon fa fa-car"></span>Diesel</li>
                                <li><span class="icon fa fa-clock-o"></span>2014</li>
                                <li><span class="icon fa fa-gear"></span>Automatic</li>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <!--Offer Block-->
                <div class="offer-block">
                	<div class="inner-box">
                    	<div class="image">
                        	<a href="#"><img src="images/resource/car-17.png" alt="" /></a>
                        </div>
                        <h3><a href="#">Volkswagen Touareg Personal Luxuary Car</a></h3>
                        <div class="lower-box">
                        	<div class="plus-box">
                            	<span class="icon fa fa-plus"></span>
                                <ul class="tooltip-data">
                                	<li>5 Years Warranty</li>
                                    <li>Free Registration</li>
                                    <li>Free 2 Years Services</li>
                                </ul>
                            </div>
                        	<div class="lower-price">$78900</div>
                            <ul>
                            	<li><span class="icon fa fa-road"></span>41500</li>
                                <li><span class="icon fa fa-car"></span>petrol</li>
                                <li><span class="icon fa fa-clock-o"></span>2010</li>
                                <li><span class="icon fa fa-gear"></span>Manual</li>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <!--Offer Block-->
                <div class="offer-block">
                	<div class="inner-box">
                    	<div class="image">
                        	<a href="#"><img src="images/resource/car-18.png" alt="" /></a>
                        </div>
                        <h3><a href="#">Renault Dauphine Campact car Sedan</a></h3>
                        <div class="lower-box">
                        	<div class="plus-box">
                            	<span class="icon fa fa-plus"></span>
                                <ul class="tooltip-data">
                                	<li>5 Years Warranty</li>
                                    <li>Free Registration</li>
                                    <li>Free 2 Years Services</li>
                                </ul>
                            </div>
                        	<div class="lower-price">$56000</div>
                            <ul>
                            	<li><span class="icon fa fa-road"></span>15000</li>
                                <li><span class="icon fa fa-car"></span>Deisel</li>
                                <li><span class="icon fa fa-clock-o"></span>2015</li>
                                <li><span class="icon fa fa-gear"></span>Automatic</li>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <!--Offer Block-->
                <div class="offer-block">
                	<div class="inner-box">
                    	<div class="image">
                        	<a href="#"><img src="images/resource/car-16.png" alt="" /></a>
                            <div class="price">1.5 <span class="percent">% <br> APR</span></div>
                        </div>
                        <h3><a href="#">BMW F12 6 Series Midsized <br> Convertible</a></h3>
                        <div class="lower-box">
                        	<div class="plus-box">
                            	<span class="icon fa fa-plus"></span>
                                <ul class="tooltip-data">
                                	<li>5 Years Warranty</li>
                                    <li>Free Registration</li>
                                    <li>Free 2 Years Services</li>
                                </ul>
                            </div>
                        	<div class="lower-price">$65000</div>
                            <ul>
                            	<li><span class="icon fa fa-road"></span>26000</li>
                                <li><span class="icon fa fa-car"></span>Diesel</li>
                                <li><span class="icon fa fa-clock-o"></span>2014</li>
                                <li><span class="icon fa fa-gear"></span>Automatic</li>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <!--Offer Block-->
                <div class="offer-block">
                	<div class="inner-box">
                    	<div class="image">
                        	<a href="#"><img src="images/resource/car-17.png" alt="" /></a>
                        </div>
                        <h3><a href="#">Volkswagen Touareg Personal Luxuary Car</a></h3>
                        <div class="lower-box">
                        	<div class="plus-box">
                            	<span class="icon fa fa-plus"></span>
                                <ul class="tooltip-data">
                                	<li>5 Years Warranty</li>
                                    <li>Free Registration</li>
                                    <li>Free 2 Years Services</li>
                                </ul>
                            </div>
                        	<div class="lower-price">$78900</div>
                            <ul>
                            	<li><span class="icon fa fa-road"></span>41500</li>
                                <li><span class="icon fa fa-car"></span>petrol</li>
                                <li><span class="icon fa fa-clock-o"></span>2010</li>
                                <li><span class="icon fa fa-gear"></span>Manual</li>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <!--Offer Block-->
                <div class="offer-block">
                	<div class="inner-box">
                    	<div class="image">
                        	<a href="#"><img src="images/resource/car-18.png" alt="" /></a>
                        </div>
                        <h3><a href="#">Renault Dauphine Campact car Sedan</a></h3>
                        <div class="lower-box">
                        	<div class="plus-box">
                            	<span class="icon fa fa-plus"></span>
                                <ul class="tooltip-data">
                                	<li>5 Years Warranty</li>
                                    <li>Free Registration</li>
                                    <li>Free 2 Years Services</li>
                                </ul>
                            </div>
                        	<div class="lower-price">$56000</div>
                            <ul>
                            	<li><span class="icon fa fa-road"></span>15000</li>
                                <li><span class="icon fa fa-car"></span>Deisel</li>
                                <li><span class="icon fa fa-clock-o"></span>2015</li>
                                <li><span class="icon fa fa-gear"></span>Automatic</li>
                            </ul>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!--End Offer Section-->
    
    
    <!--Testimonial Section-->
    <section class="testimonial-section">
    	<div class="auto-container">
        	<!--Sec Title-->
            <div class="sec-title centered">
            	<h2>Customer Feedback</h2>
            </div>
            <div class="testimonial-carousel owl-carousel owl-theme">
            	<!--Testimonial Block-->
            	<div class="testimonial-block">
                	<div class="inner-box">
                    	<div class="image">
                        	<img src="images/resource/author-1.jpg" alt="" />
                        </div>
                        <div class="quote-icon"><span class="icon flaticon-double-quotes"></span></div>
                        <div class="text">They have got my project time with the competition with a highly skilled, well organized and experienced team professional mates praising.</div>
                        <div class="lower-box clearfix">
                        	<div class="pull-left">
                            	<h3>James Fernando</h3>
                                <div class="location">Newyork City</div>
                            </div>
                            <div class="pull-right">
                            	<div class="rating">
                                	<span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="light fa fa-star-half-o"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Testimonial Block-->
            	<div class="testimonial-block">
                	<div class="inner-box">
                    	<div class="image">
                        	<img src="images/resource/author-2.jpg" alt="" />
                        </div>
                        <div class="quote-icon"><span class="icon flaticon-double-quotes"></span></div>
                        <div class="text">How this mistaken idea denouncing pleasure and praising pain was born and will give you complete account  the system expound great.</div>
                        <div class="lower-box clearfix">
                        	<div class="pull-left">
                            	<h3>Venanda Joe</h3>
                                <div class="location">California</div>
                            </div>
                            <div class="pull-right">
                            	<div class="rating">
                                	<span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="light fa fa-star-half-o"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Testimonial Block-->
            	<div class="testimonial-block">
                	<div class="inner-box">
                    	<div class="image">
                        	<img src="images/resource/author-3.jpg" alt="" />
                        </div>
                        <div class="quote-icon"><span class="icon flaticon-double-quotes"></span></div>
                        <div class="text">There anyone who loves or pursues or desires obtain pain itself, because it  pain, but because occasionally occur  which toil and pain procure.</div>
                        <div class="lower-box clearfix">
                        	<div class="pull-left">
                            	<h3>Jimmy Macro</h3>
                                <div class="location">Los Angeles</div>
                            </div>
                            <div class="pull-right">
                            	<div class="rating">
                                	<span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="light fa fa-star-half-o"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Testimonial Block-->
            	<div class="testimonial-block">
                	<div class="inner-box">
                    	<div class="image">
                        	<img src="images/resource/author-1.jpg" alt="" />
                        </div>
                        <div class="quote-icon"><span class="icon flaticon-double-quotes"></span></div>
                        <div class="text">They have got my project time with the competition with a highly skilled, well organized and experienced team professional mates praising.</div>
                        <div class="lower-box clearfix">
                        	<div class="pull-left">
                            	<h3>James Fernando</h3>
                                <div class="location">Newyork City</div>
                            </div>
                            <div class="pull-right">
                            	<div class="rating">
                                	<span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="light fa fa-star-half-o"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Testimonial Block-->
            	<div class="testimonial-block">
                	<div class="inner-box">
                    	<div class="image">
                        	<img src="images/resource/author-2.jpg" alt="" />
                        </div>
                        <div class="quote-icon"><span class="icon flaticon-double-quotes"></span></div>
                        <div class="text">How this mistaken idea denouncing pleasure and praising pain was born and will give you complete account  the system expound great.</div>
                        <div class="lower-box clearfix">
                        	<div class="pull-left">
                            	<h3>Venanda Joe</h3>
                                <div class="location">California</div>
                            </div>
                            <div class="pull-right">
                            	<div class="rating">
                                	<span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="light fa fa-star-half-o"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Testimonial Block-->
            	<div class="testimonial-block">
                	<div class="inner-box">
                    	<div class="image">
                        	<img src="images/resource/author-3.jpg" alt="" />
                        </div>
                        <div class="quote-icon"><span class="icon flaticon-double-quotes"></span></div>
                        <div class="text">There anyone who loves or pursues or desires obtain pain itself, because it  pain, but because occasionally occur  which toil and pain procure.</div>
                        <div class="lower-box clearfix">
                        	<div class="pull-left">
                            	<h3>Jimmy Macro</h3>
                                <div class="location">Los Angeles</div>
                            </div>
                            <div class="pull-right">
                            	<div class="rating">
                                	<span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="light fa fa-star-half-o"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Testimonial Block-->
            	<div class="testimonial-block">
                	<div class="inner-box">
                    	<div class="image">
                        	<img src="images/resource/author-1.jpg" alt="" />
                        </div>
                        <div class="quote-icon"><span class="icon flaticon-double-quotes"></span></div>
                        <div class="text">They have got my project time with the competition with a highly skilled, well organized and experienced team professional mates praising.</div>
                        <div class="lower-box clearfix">
                        	<div class="pull-left">
                            	<h3>James Fernando</h3>
                                <div class="location">Newyork City</div>
                            </div>
                            <div class="pull-right">
                            	<div class="rating">
                                	<span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="light fa fa-star-half-o"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Testimonial Block-->
            	<div class="testimonial-block">
                	<div class="inner-box">
                    	<div class="image">
                        	<img src="images/resource/author-2.jpg" alt="" />
                        </div>
                        <div class="quote-icon"><span class="icon flaticon-double-quotes"></span></div>
                        <div class="text">How this mistaken idea denouncing pleasure and praising pain was born and will give you complete account  the system expound great.</div>
                        <div class="lower-box clearfix">
                        	<div class="pull-left">
                            	<h3>Venanda Joe</h3>
                                <div class="location">California</div>
                            </div>
                            <div class="pull-right">
                            	<div class="rating">
                                	<span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="light fa fa-star-half-o"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Testimonial Block-->
            	<div class="testimonial-block">
                	<div class="inner-box">
                    	<div class="image">
                        	<img src="images/resource/author-3.jpg" alt="" />
                        </div>
                        <div class="quote-icon"><span class="icon flaticon-double-quotes"></span></div>
                        <div class="text">There anyone who loves or pursues or desires obtain pain itself, because it  pain, but because occasionally occur  which toil and pain procure.</div>
                        <div class="lower-box clearfix">
                        	<div class="pull-left">
                            	<h3>Jimmy Macro</h3>
                                <div class="location">Los Angeles</div>
                            </div>
                            <div class="pull-right">
                            	<div class="rating">
                                	<span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="light fa fa-star-half-o"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!--End Testimonial Section-->
    
    <?php include("include/footer.php")?>
    
    
</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="icon fa fa-angle-up"></span></div>

<script src="js/jquery.js"></script> 
<script src="js/bootstrap.min.js"></script>
<!--Revolution Slider-->
<script src="plugins/revolution/js/jquery.themepunch.revolution.min.js"></script>
<script src="plugins/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.migration.min.js"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.video.min.js"></script>
<script src="js/main-slider-script.js"></script>
<!--End Revolution Slider-->
<script src="js/jquery-ui.js"></script>
<script src="js/jquery.fancybox.pack.js"></script>
<script src="js/jquery.fancybox-media.js"></script>
<script src="js/owl.js"></script>
<script src="js/appear.js"></script>
<script src="js/wow.js"></script>
<script src="js/main-slider-script.js"></script>
<script src="js/script.js"></script>

</body>
</html>
