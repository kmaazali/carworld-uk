<?php
	include("include/config.php");
							
		$id=@$_GET['id'];
	
		$sql ="SELECT * FROM addcar WHERE id='$id'";
					
		$result = mysqli_query ($conn,$sql);
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Car</title>
<!-- Stylesheets -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/jquery.datetimepicker.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">

<!--Favicon-->
<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
<link rel="icon" href="images/favicon.png" type="image/x-icon">
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>

<body>
<div class="page-wrapper">
 	
    
 	<?php include("include/header.php");?>
   
    <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/5.jpg);">
        <div class="auto-container">
            <h1>Inventory Single</h1>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Page Info-->
    <section class="page-info">
        <div class="auto-container">
            <ul class="bread-crumb">
                <li><a href="index.html">Home</a></li>
                <li>Pages</li>
                <li class="current">Inventory Single</li>
            </ul>
        </div>
    </section>
    <!--End Page Info-->
    
    <!--Inventory Section-->
    <section class="inventory-section inventory-single">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <!--Column-->
            	<div class="column col-lg-9 col-md-8 col-sm-12 col-xs-12">
                	<!--Inventory Details-->
                    <div class="inventory-details">
						<?php 
							
								while ($row = mysqli_fetch_assoc ($result)){
									 			
						?>
                        <!--Product Carousel-->
                        <div class="product-carousel-outer">
                            <div class="big-image-outer">
                            
                                
                                <!--Product image Carousel-->
                                <ul class="prod-image-carousel owl-theme owl-carousel">
                                    <li><figure class="image"><img src="admin/uploads/inventory-image.jpg" alt=""><a class="lightbox-image fancy-btn" data-fancybox-group="example-gallery" href="images/resource/inventory-image-7.jpg" title="Image Title Here"><span class="fa fa-search-plus"></span></a></figure></li>
                                </ul>
                            </div>
                            
                            <!--Product Thumbs Carousel-->
                            <div class="prod-thumbs-carousel owl-theme owl-carousel">
                                <div class="thumb-item"><figure class="thumb-box"><img src="images/resource/inv-thumb-1.jpg" alt=""></figure></div>
                                <div class="thumb-item"><figure class="thumb-box"><img src="images/resource/inv-thumb-2.jpg" alt=""></figure></div>
                                <div class="thumb-item"><figure class="thumb-box"><img src="images/resource/inv-thumb-3.jpg" alt=""><div class="video-icon"><span class="fa fa-play"></span></div></figure></div>
                                <div class="thumb-item"><figure class="thumb-box"><img src="images/resource/inv-thumb-4.jpg" alt=""></figure></div>
                                <div class="thumb-item"><figure class="thumb-box"><img src="images/resource/inv-thumb-5.jpg" alt=""></figure></div>
                                <div class="thumb-item"><figure class="thumb-box"><img src="images/resource/inv-thumb-1.jpg" alt=""></figure></div>
                                <div class="thumb-item"><figure class="thumb-box"><img src="images/resource/inv-thumb-2.jpg" alt=""></figure></div>
                                <div class="thumb-item"><figure class="thumb-box"><img src="images/resource/inv-thumb-3.jpg" alt=""><div class="video-icon"><span class="fa fa-play"></span></div></figure></div>
                                <div class="thumb-item"><figure class="thumb-box"><img src="images/resource/inv-thumb-4.jpg" alt=""></figure></div>
                                <div class="thumb-item"><figure class="thumb-box"><img src="images/resource/inv-thumb-5.jpg" alt=""></figure></div>
                            </div>
                            
                        </div><!--End Product Carousel-->
                        
						
						
                        <!--Vehicle Details-->
                        <div class="vehicle-details">
                        	<div class="text-description">
                            	<h2><?php echo $row['name']; ?></h2>
                                <div class="text">
                                	<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using content here, content heremaking it look like readable Englishbut the majority haveor randomised words which don't look even slightly believable. </p>
                                    <p>Distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using content herehave suffered alteration.</p>
                                </div>
                            </div>
                            
                            <div class="images">
                            	<div class="row clearfix">
                                	<div class="image-column col-md-5 col-sm-4 col-xs-12">
                                    	<figure class="image"><img src="images/resource/inventory-image-8.jpg" alt=""></figure>
                                    </div>
                                    <div class="image-column col-md-7 col-sm-8 col-xs-12">
                                    	<figure class="image"><img src="images/resource/inventory-image-9.jpg" alt=""></figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <!--Details Panel Box-->
                        <div class="details-panel-box">
                        	<div class="panel-header"><div class="panel-btn clearfix"><h4><strong>Key Features</strong> of <?php echo $row['name']; ?> </h4><div class="arrow"><span class="fa fa-angle-down"></span></div></div></div>
                            <div class="panel-content">
                            	<div class="content-box">
                                	<div class="listing-outer clearfix">
                                    	<div class="list-column">
                                        	<ul class="list-style-seven">
                                            	<li class="clearfix"><span class="ttl">Reg</span><span class="dtl"><?php echo $row['reg']; ?></span></li>
                                                <li class="clearfix"><span class="ttl">Make</span><span class="dtl"><?php echo $row['make']; ?></span></li>
                                                <li class="clearfix"><span class="ttl">Gearbox</span><span class="dtl"><?php echo $row['gear']; ?></span></li>
                                                <li class="clearfix"><span class="ttl">Doors</span><span class="dtl"><?php echo $row['door']; ?></span></li>
                                                
                                            </ul>
                                        </div>
                                        <div class="list-column">
                                        	<ul class="list-style-seven">
                                            	<li class="clearfix"><span class="ttl">MileAge</span><span class="dtl"><?php echo $row['mile']; ?></span></li>
												<li class="clearfix"><span class="ttl">Model</span><span class="dtl"><?php echo $row['model']; ?></span></li>
												<li class="clearfix"><span class="ttl">Fuel</span><span class="dtl"><?php echo $row['fuel']; ?></span></li>
												<li class="clearfix"><span class="ttl">Color</span><span class="dtl"><?php echo $row['color']; ?></span></li>
												
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <!--Details Panel Box-->
                        <div class="details-panel-box extra-features">
                        	<div class="panel-header"><div class="panel-btn clearfix"><h4><strong>Extra Features </strong> of Audi A8 3.0 TDI S12 Quattro Tiptronic </h4><div class="arrow"><span class="fa fa-angle-down"></span></div></div></div>
                            <div class="panel-content">
                            	<div class="content-box">
                                    <div class="listing-outer clearfix">
                                        <div class="list-column">
                                            <ul class="list-style-nine">
                                                <li>Auto on Headlights</li>
                                                <li>LED Taillights</li>
                                                <li>Solar Tinted Glass</li>
                                                <li>Spare Tire Kit (Inflator)</li>
                                            </ul>
                                        </div>
                                        <div class="list-column">
                                            <ul class="list-style-nine">
                                                <li>Sensor Airbags (Approved)</li>
                                                <li>Child Seat Anchors</li>
                                                <li>Anti Theft System (Alarm)</li>
                                                <li>Advanced Bluetooth</li>
                                            </ul>
                                        </div>
                                        <div class="list-column">
                                            <ul class="list-style-nine">
                                                <li>CD & DVD Players</li>
                                                <li>Electric Side Mirror</li>
                                                <li>Navigation System</li>
                                                <li>Rear Traffic Alert</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                     
                        
                    </div>
                </div>
                
                <!--Form Column-->
                <div class="form-column col-lg-3 col-md-4 col-sm-12 col-xs-12">
                	<!--Loan Calculator Widget-->
                    <div class="loan-cal-widget">
                    	<div class="inner">
                    		<h3>Get a Qoute</h3>
                            <div class="form-box">
                            	<!--Cars Form-->
                                <div class="cars-form">
                                    <form method="post" action="include/process.php">
                                        <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
										<input type="hidden" name="pname" value="<?php echo $row['name']; ?>">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="text" name="cname" placeholder="Name" >
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="text" name="cemail" placeholder="Email" >
                                        </div>
										<div class="form-group">
                                            <label>Number</label>
                                            <input type="number" name="cnum" placeholder="Contact Number" >
                                        </div>
                                                
                                        <div class="row clearfix">
                                            <div class="form-group inner-group col-md-6 col-sm-6 col-xs-12">
                                                <label>Postcode</label>
                                                <input type="text" name="cpc" placeholder="Poscode">
                                            </div>
                                            <div class="form-group inner-group col-md-6 col-sm-6 col-xs-12">
                                                <label>City</label>
                                                <input type="text" name="ccity" placeholder="City" >
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <button class="theme-btn btn-style-one" type="submit" name="send">Send</button>
                                            <button class="theme-btn btn-style-one" type="submit" name="send" style="font-size:12px;" >View Car At Home</button>
											
                                        </div>
                                        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    
                    
                    
                </div>
				<?php
						}
						?>
            </div>
        </div>
    </section>
    <!--End Inventory Section-->
    
	<?php include("include/footer.php");?>
    
</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="icon fa fa-angle-up"></span></div>

<script src="js/jquery.js"></script> 
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.fancybox.pack.js"></script>
<script src="js/jquery.fancybox-media.js"></script>
<script src="js/owl.js"></script>
<script src="js/appear.js"></script>
<script src="js/jquery.datetimepicker.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/wow.js"></script>
<script src="js/script.js"></script>

</body>
</html>
