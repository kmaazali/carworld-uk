<!-- Main Header-->
    <header class="main-header">
        
        <!--Header Top-->
        <div class="header-top">
            <div class="auto-container">
            	<div class="clearfix">
                	<!--Top Left-->
                    <div class="top-left">
                    	
                        <div class="social-links">
                        	<a href="#"><span class="fa fa-facebook-f"></span></a>
                            <a href="#"><span class="fa fa-twitter"></span></a>
                            <a href="#"><span class="fa fa-linkedin"></span></a>
                            
                            
                        </div>
                    </div>
                    <!--Top Right-->
                    <div class="top-right">
                    	<ul class="dropdown-option clearfix">
                        	
                            <li><a class="sell-car" href="sell-car.php">Sell Your Car</a></li>
							<li><a class="sell-car" href="#">Buy New Car</a></li>
							<li><a class="sell-car" href="value-my-car.php">Value My Car</a></li>
                        </ul>
                    </div>
                    
                </div>
            </div>
        </div>
        
        <!--Header-Upper-->
        <div class="header-upper">
        	<div class="auto-container">
            	<div class="upper-inner clearfix">
                	
                	<div class="logo-outer">
                    	<div class="logo"><a href="index.html"><img src="images/logo.png" alt="" title=""></a></div>
                    </div>
                    
                    <div class="upper-right clearfix">
                    	
                        <div class="nav-outer clearfix">
                            <!-- Main Menu -->
                            <nav class="main-menu">
                                <div class="navbar-header">
                                    <!-- Toggle Button -->    	
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    </button>
                                </div>
                                
                                <div class="navbar-collapse collapse clearfix">
                                    <ul class="navigation clearfix">
                                        <li class="current"><a href="index.php">Home</a></li>
                                        
                                        <li class="dropdown"><a href="#">Buy a Car</a>
                                            <ul>
                                                <li><a href="new-car.php">New Car</a></li>
                                                <li><a href="used-car.php">Used Car</a></li>
                                                
                                            </ul>
                                        </li>
										<li class="dropdown"><a href="#">Sell your Car</a>
                                            <ul>
                                                <li><a href="sell-car.php">Sell your Car</a></li>
												<li><a href="#">Part Exchange</a></li>
                                                <li><a href="value-my-car.php">Value My Car</a></li>
                                                
                                            </ul>
                                        </li>
                                        <li><a href="about.php">About Us</a></li>
                                        
                                       
                                        <li><a href="contact.php">Contact</a></li>
                                    </ul>
                                </div>
                            </nav>
                            <!-- Main Menu End-->
                            
                            
                            
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>
        <!--End Header Upper-->
        
        <div class="header-lower">
        	<div class="auto-container">
                <div class="lower-inner">
                    <ul class="info-block">
                        <li><span class="icon flaticon-maps-and-flags"></span>Carworld, Newyork 10012, USA</li>
                        <li><span class="icon flaticon-telephone"></span>Phone: +44 567 890123</li>
                        <li><span class="icon flaticon-timer"></span>Mon-Satday: 09.00am to 18.00pm</li>
						
                    </ul>
                    
                </div>
            </div>
        </div>
        
        <!--Sticky Header-->
        <div class="sticky-header">
            <div class="auto-container clearfix">
                <!--Logo-->
                <div class="logo pull-left">
                    <a href="index.html" class="img-responsive" title="Car World"><img src="images/logo-small.png" alt="Tali" title="Car World"></a>
                </div>
                
                <!--Right Col-->
                <div class="right-col pull-right">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-header">
                            <!-- Toggle Button -->    	
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            </button>
                        </div>
                        
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li class="current"><a href="index.php">Home</a></li>
                                
                                <li class="dropdown"><a href="#">Buy a Car</a>
                                            <ul>
                                                <li><a href="new-car.php">New Car</a></li>
                                                <li><a href="used-car.php">Used Car</a></li>
                                                
                                            </ul>
                                        </li>
										<li class="dropdown"><a href="#">Sell your Car</a>
                                            <ul>
                                                <li><a href="sell-car.php">Sell your Car</a></li>
												<li><a href="#">Part Exchange</a></li>
                                                <li><a href="value-my-car.php">Value My Car</a></li>
                                                
                                            </ul>
                                        </li>
                                        <li><a href="about.php">About Us</a></li>
                                        
                                       
                                        <li><a href="contact.php">Contact</a></li>
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>
                
            </div>
        </div>
        <!--End Sticky Header-->
    
    </header>
    <!--End Main Header -->
    