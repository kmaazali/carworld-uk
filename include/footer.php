<!--Main Footer-->
    <footer class="main-footer" style="background-image:url(images/background/4.png);">
		<div class="auto-container">
        	<!--Widgets Section-->
            <div class="widgets-section">
            	<div class="row clearfix">
                	
                    <!--big column-->
                    <div class="big-column col-md-6 col-sm-12 col-xs-12">
                        <div class="row clearfix">
                        
                            <!--Footer Column-->
                            <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                <div class="footer-widget about-widget">
									<h2>About Us</h2>
                                    <div class="text">
                                    	<p>Must explain to  how all this mistaken idea of denouncing pleasure & praising pain was born and system.</p>
                                        <p>There anyone who loves or pursues or desires to obtain pain  itself, because it is pain, but because occasionally occur in whichgreat pleasure. </p>
                                    </div>
                                    <a href="about.html" class="theme-btn btn-style-three">Read More</a>
								</div>
                            </div>
                            
                            <!--Footer Column-->
                            <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                <div class="footer-widget links-widget">
									<h2>Usefull Links</h2>
                                    <div class="widget-content">
                                    	<div class="row clearfix">
                                        	<div class="col-md-6 col-sm-6 col-xs-12">
                                            	<ul class="footer-links">
                                                	<li><a href="new-car.php">Buy New Car</a></li>
                                                    <li><a href="used-car.php">Buy Used Car</a></li>
                                                    <li><a href="#">Sell Your Car</a></li>
                                                    <li><a href="#">Value My Car</a></li>
                                                    <li><a href="#">Part Exchange</a></li>
                                                    <li><a href="#">Scrap My Car</a></li>
                                                    
                                                </ul>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                            	<ul class="footer-links">
                                                	<li><a href="index.php">Home</a></li>
                                                    <li><a href="about.php">About Us</a></li>
                                                    <li><a href="contact.php">Contact Us</a></li>
                                                    <li><a href="#">Inventory</a></li>
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    
                    <!--big column-->
                    <div class="big-column col-md-6 col-sm-12 col-xs-12">
                        <div class="row clearfix">
                        
                        	<!--Footer Column-->
                            <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                <div class="footer-widget links-widget">
									<h2>Contact Details</h2>
                                    <div class="widget-content">
                                    	<ul class="list-style-one">
                                        	<li><span class="icon flaticon-maps-and-flags"></span>Carworld, Newyork 10012, USA</li>
                                            <li><span class="icon flaticon-telephone"></span>Phone: +44 567 890123 & 45</li>
                                            <li><span class="icon flaticon-fax"></span>Fax: (526) 326-985-7423</li>
                                            <li><span class="icon flaticon-web"></span>Supportteam@Carworld.com</li>
                                        </ul>
                                    </div>
                                    <h2>Working Hours</h2>
                                    <div class="text">Monday to Friday: 08.00 am to 18.00pm Saturday & Sunday: <a href="#">Closed</a></div>
                                </div>
                            </div>
                        
                        	<!--Footer Column-->
                            <div class="footer-column col-md-6 col-sm-6 col-xs-12" style="display:none;">
                                <div class="footer-widget offer-widget">
									<h2>Special Offers</h2>
                                    <div class="widget-content">
                                    	<div class="single-item-carousel owl-carousel owl-dots">
                                        
                                        	<!--Offer Block-->
                                        	<div class="offer-block">
                                            	<div class="inner">
                                                	<h3>Service Specials Includes:</h3>
                                                    <ul class="list-style-two">
                                                    	<li>Change oil filter</li>
                                                        <li>Resurface rotors</li>
                                                        <li>Inspect steering likage</li>
                                                        <li>Inspect & rotate tires</li>
                                                    </ul>
                                                    <div class="price"><span>Only <br> For</span><sup>$</sup>59.50</div>
                                                </div>
                                            </div>
                                            
                                            <!--Offer Block-->
                                        	<div class="offer-block">
                                            	<div class="inner">
                                                	<h3>Service Specials Includes:</h3>
                                                    <ul class="list-style-two">
                                                    	<li>Change oil filter</li>
                                                        <li>Resurface rotors</li>
                                                        <li>Inspect steering likage</li>
                                                        <li>Inspect & rotate tires</li>
                                                    </ul>
                                                    <div class="price"><span>Only <br> For</span><sup>$</sup>59.50</div>
                                                </div>
                                            </div>
                                            
                                            <!--Offer Block-->
                                        	<div class="offer-block">
                                            	<div class="inner">
                                                	<h3>Service Specials Includes:</h3>
                                                    <ul class="list-style-two">
                                                    	<li>Change oil filter</li>
                                                        <li>Resurface rotors</li>
                                                        <li>Inspect steering likage</li>
                                                        <li>Inspect & rotate tires</li>
                                                    </ul>
                                                    <div class="price"><span>Only <br> For</span><sup>$</sup>59.50</div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!--Footer Bottom-->
        <div class="footer-bottom">
        	<div class="auto-container">
            	<div class="row clearfix">
                	<div class="col-md-6 col-sm-6 col-xs-12">
                    	<div class="copyright">Copyright © 2018 All Rights Reserved by <a href="#">Kashan Rasheed</a></div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12" style="display:none;">
                    	<ul class="footer-nav">
                        	<li><a href="index.html">Home</a></li>
                            <li><a href="#">About</a></li>
                            <li><a href="#">Policies</a></li>
                            <li><a href="#">Buy</a></li>
                            <li><a href="#">Sell</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--End Main Footer-->
    