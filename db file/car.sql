-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 26, 2018 at 10:20 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `car`
--

-- --------------------------------------------------------

--
-- Table structure for table `addcar`
--

CREATE TABLE `addcar` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `reg` varchar(250) NOT NULL,
  `mile` int(11) NOT NULL,
  `make` varchar(250) NOT NULL,
  `model` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `color` varchar(250) NOT NULL,
  `fuel` varchar(250) NOT NULL,
  `gear` varchar(250) NOT NULL,
  `door` varchar(250) NOT NULL,
  `seat` varchar(250) NOT NULL,
  `acc` int(11) NOT NULL,
  `speed` int(11) NOT NULL,
  `abdriver` varchar(250) NOT NULL,
  `abpass` varchar(250) NOT NULL,
  `condition` varchar(250) NOT NULL,
  `image` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `addcar`
--

INSERT INTO `addcar` (`id`, `name`, `reg`, `mile`, `make`, `model`, `price`, `color`, `fuel`, `gear`, `door`, `seat`, `acc`, `speed`, `abdriver`, `abpass`, `condition`, `image`) VALUES
(1, 'name', 'reg', 1, 'make', 1, 1, 'color', 'fuel', 'gear', 'door', 'seat', 1, 1, 'ab', 'ab', 'con', 'image'),
(4, 'name', 'regg', 1, 'make', 1, 1, 'color', 'fuel', 'gear', 'door', 'seat', 1, 1, 'ab', 'ab', 'con', 'image'),
(5, 'Title', 'ewew', 10, 'Honda', 2019, 1, 'Red', 'bifuel', 'auto', 'four', 'four', 1, 1, 'yes', 'yes', 'new', 'image'),
(6, 'Title', 'tytu', 10, 'Honda', 2020, 50, 'Red', 'petrol', 'hybrid', 'five', 'five', 1, 1, 'no', 'yes', 'used', 'image'),
(7, 'Abc', 'sdsd', 10, 'Honda', 3445, 50, 'Red', 'electric', 'manual', 'five', 'five', 10, 10, 'no', 'yes', 'new', '5bfc3605db3c7.png'),
(8, 'Volkswagen Polo 1.4 CL Hatchback 5dr Petrol Manual', 'T926SRE', 118000, 'Make', 1999, 295, 'Red', 'Electric', 'Manual', 'Five', 'Four', 15, 99, 'Yes', 'Yes', 'Used', '5bfc46d8d5a11.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `cat-name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `cat-name`) VALUES
(1, 'kashan');

-- --------------------------------------------------------

--
-- Table structure for table `enquiry`
--

CREATE TABLE `enquiry` (
  `id` int(11) NOT NULL,
  `cname` varchar(250) NOT NULL,
  `cemail` varchar(250) NOT NULL,
  `cnumber` bigint(20) NOT NULL,
  `cpostcode` text NOT NULL,
  `ccity` varchar(250) NOT NULL,
  `pname` varchar(250) NOT NULL,
  `link` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `enquiry`
--

INSERT INTO `enquiry` (`id`, `cname`, `cemail`, `cnumber`, `cpostcode`, `ccity`, `pname`, `link`) VALUES
(1, 'cname', 'abc@abc.com', 3783, 'd43f', 'heef', '', ''),
(2, 'kashan rasheed', 'kashanrasheed44@gmail.com', 0, '', 'islamabad', '', ''),
(3, 'kashan rasheed', 'kashanrasheed44@gmail.com', 0, '', 'islamabad', '', ''),
(4, 'kashan ', 'kashanrasheed44@gmail.com', 434, 'fr4r', 'islamabad', '', ''),
(5, 'A', 'kashanrasheed44@gmail.com', 122133, 'fred', 'isb', '', ''),
(6, 'A', 'kashanrasheed44@gmail.com', 122133, 'fred', 'isb', '', ''),
(7, 'rasheed', 'kashanrasheed44@gmail.com', 466, '46000', 'islamabad', 'Volkswagen Polo 1.4 CL Hatchback 5dr Petrol Manual', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addcar`
--
ALTER TABLE `addcar`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `reg` (`reg`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cat-name` (`cat-name`);

--
-- Indexes for table `enquiry`
--
ALTER TABLE `enquiry`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addcar`
--
ALTER TABLE `addcar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `enquiry`
--
ALTER TABLE `enquiry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
