<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>CarWorld HTML Template | Inventory 01</title>
<!-- Stylesheets -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">

<!--Favicon-->
<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
<link rel="icon" href="images/favicon.png" type="image/x-icon">
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>

<body>
<div class="page-wrapper">
 	
    <?php include("include/header.php");?>
    
    <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/5.jpg);">
        <div class="auto-container">
            <h1>Inventory Style 01</h1>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Page Info-->
    <section class="page-info">
        <div class="auto-container">
            <ul class="bread-crumb">
                <li><a href="index.html">Home</a></li>
                <li>Pages</li>
                <li class="current">Inventory Style 01</li>
            </ul>
        </div>
    </section>
    <!--End Page Info-->
    
    <!--Inventory Section-->
    <section class="inventory-section">
    	<div class="auto-container">
        	<div class="row clearfix">
            	<!--Column-->
            	<div class="column col-lg-9 col-md-8 col-sm-12 col-xs-12">
                	<div class="layout-box clearfix">
                    	<div class="pull-left">
                        	<div class="sort-form">
                                <form action="inventory-form">
                                    <div class="form-group">
                                        <label>Sort By:</label>
                                        <select class="custom-select-box">
                                            <option>Price: Highest First</option>
                                            <option>Second</option>
                                            <option>Third</option>
                                            <option>Fourth</option>
                                            <option>Five</option>
                                        </select>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="pull-right">
                        	<ul class="view-options">
                            	<li class="active"><a href="inventory-1.html"><span class="fa fa-th-large"></span></a></li>
                                <li><a href="inventory-2.html"><span class="flaticon-list"></span></a></li>
                            </ul>
                        </div>
                    </div>
                    
                    <!--End Sec Title-->
                    <div class="row clearfix">
                        <!--Car Block-->
                        <div class="car-block col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div class="inner-box">
                                <div class="image">
                                    <a href="inventory-single.html"><img src="images/resource/car-1.png" alt="" /></a>
                                    <div class="price">$42000</div>
                                </div>
                                <h3><a href="inventory-single.html">BMW F12 6 Convertible</a></h3>
                                <div class="lower-box">
                                    <ul class="car-info">
                                        <li><span class="fa fa-road icon"></span>26000</li>
                                        <li><span class="icon fa fa-car"></span>Diesel</li>
                                        <li><span class="icon fa fa-clock-o"></span>2014</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                        <!--Car Block-->
                        <div class="car-block col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div class="inner-box">
                                <div class="image">
                                    <a href="inventory-single.html"><img src="images/resource/car-2.png" alt="" /></a>
                                    <div class="price">$35500</div>
                                </div>
                                <h3><a href="inventory-single.html">Audi A8 3.0 TDI S12</a></h3>
                                <div class="lower-box">
                                    <ul class="car-info">
                                        <li><span class="fa fa-road icon"></span>18000</li>
                                        <li><span class="icon fa fa-car"></span>Petrol</li>
                                        <li><span class="icon fa fa-clock-o"></span>2008</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                        <!--Car Block-->
                        <div class="car-block col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div class="inner-box">
                                <div class="image">
                                    <a href="inventory-single.html"><img src="images/resource/car-3.png" alt="" /></a>
                                    <div class="price">$24000</div>
                                </div>
                                <h3><a href="inventory-single.html">Ford Fiesta Hatchback</a></h3>
                                <div class="lower-box">
                                    <ul class="car-info">
                                        <li><span class="fa fa-road icon"></span>32500</li>
                                        <li><span class="icon fa fa-car"></span>Diesel</li>
                                        <li><span class="icon fa fa-clock-o"></span>2005</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                        <!--Car Block-->
                        <div class="car-block col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div class="inner-box">
                                <div class="image">
                                    <a href="inventory-single.html"><img src="images/resource/car-5.png" alt="" /></a>
                                    <div class="price">$23750</div>
                                </div>
                                <h3><a href="inventory-single.html">Caterham 7 Superlight</a></h3>
                                <div class="lower-box">
                                    <ul class="car-info">
                                        <li><span class="fa fa-road icon"></span>32500</li>
                                        <li><span class="icon fa fa-car"></span>Diesel</li>
                                        <li><span class="icon fa fa-clock-o"></span>2010</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                        <!--Car Block-->
                        <div class="car-block col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div class="inner-box">
                                <div class="image">
                                    <a href="inventory-single.html"><img src="images/resource/car-6.png" alt="" /></a>
                                    <div class="price">$15000</div>
                                </div>
                                <h3><a href="inventory-single.html">Mercedes Benz c Class</a></h3>
                                <div class="lower-box">
                                    <ul class="car-info">
                                        <li><span class="fa fa-road icon"></span>26000</li>
                                        <li><span class="icon fa fa-car"></span>Diesel</li>
                                        <li><span class="icon fa fa-clock-o"></span>2014</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                        <!--Car Block-->
                        <div class="car-block col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div class="inner-box">
                                <div class="image">
                                    <a href="inventory-single.html"><img src="images/resource/car-7.png" alt="" /></a>
                                    <div class="price">$32500</div>
                                </div>
                                <h3><a href="inventory-single.html">ferrari F12 Novitec</a></h3>
                                <div class="lower-box">
                                    <ul class="car-info">
                                        <li><span class="fa fa-road icon"></span>15000</li>
                                        <li><span class="icon fa fa-car"></span>Diesel</li>
                                        <li><span class="icon fa fa-clock-o"></span>2002</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                        <!--Car Block-->
                        <div class="car-block col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div class="inner-box">
                                <div class="image">
                                    <a href="inventory-single.html"><img src="images/resource/car-8.png" alt="" /></a>
                                    <div class="price">$42000</div>
                                </div>
                                <h3><a href="inventory-single.html">BMW F12 6 Convertible</a></h3>
                                <div class="lower-box">
                                    <ul class="car-info">
                                        <li><span class="fa fa-road icon"></span>26000</li>
                                        <li><span class="icon fa fa-car"></span>Diesel</li>
                                        <li><span class="icon fa fa-clock-o"></span>2014</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                        <!--Car Block-->
                        <div class="car-block col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div class="inner-box">
                                <div class="image">
                                    <a href="inventory-single.html"><img src="images/resource/car-4.png" alt="" /></a>
                                    <div class="price">$35500</div>
                                </div>
                                <h3><a href="inventory-single.html">Audi A8 3.0 TDI S12</a></h3>
                                <div class="lower-box">
                                    <ul class="car-info">
                                        <li><span class="fa fa-road icon"></span>18000</li>
                                        <li><span class="icon fa fa-car"></span>Petrol</li>
                                        <li><span class="icon fa fa-clock-o"></span>2008</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                        <!--Car Block-->
                        <div class="car-block col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div class="inner-box">
                                <div class="image">
                                    <a href="inventory-single.html"><img src="images/resource/car-21.jpg" alt="" /></a>
                                    <div class="price">$24000</div>
                                </div>
                                <h3><a href="inventory-single.html">Ford Fiesta Hatchback</a></h3>
                                <div class="lower-box">
                                    <ul class="car-info">
                                        <li><span class="fa fa-road icon"></span>32500</li>
                                        <li><span class="icon fa fa-car"></span>Diesel</li>
                                        <li><span class="icon fa fa-clock-o"></span>2005</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                        <!--Car Block-->
                        <div class="car-block col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div class="inner-box">
                                <div class="image">
                                    <a href="inventory-single.html"><img src="images/resource/car-22.jpg" alt="" /></a>
                                    <div class="price">$23750</div>
                                </div>
                                <h3><a href="inventory-single.html">Caterham 7 Superlight</a></h3>
                                <div class="lower-box">
                                    <ul class="car-info">
                                        <li><span class="fa fa-road icon"></span>35500</li>
                                        <li><span class="icon fa fa-car"></span>Diesel</li>
                                        <li><span class="icon fa fa-clock-o"></span>2005</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                        <!--Car Block-->
                        <div class="car-block col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div class="inner-box">
                                <div class="image">
                                    <a href="inventory-single.html"><img src="images/resource/car-23.jpg" alt="" /></a>
                                    <div class="price">$32500</div>
                                </div>
                                <h3><a href="inventory-single.html">ferrari F12 Novitec</a></h3>
                                <div class="lower-box">
                                    <ul class="car-info">
                                        <li><span class="fa fa-road icon"></span>15500</li>
                                        <li><span class="icon fa fa-car"></span>Diesel</li>
                                        <li><span class="icon fa fa-clock-o"></span>2002</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                        <!--Car Block-->
                        <div class="car-block col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div class="inner-box">
                                <div class="image">
                                    <a href="inventory-single.html"><img src="images/resource/car-24.jpg" alt="" /></a>
                                    <div class="price">$15000</div>
                                </div>
                                <h3><a href="inventory-single.html">Mercedes Benz c Class</a></h3>
                                <div class="lower-box">
                                    <ul class="car-info">
                                        <li><span class="fa fa-road icon"></span>26000</li>
                                        <li><span class="icon fa fa-car"></span>Diesel</li>
                                        <li><span class="icon fa fa-clock-o"></span>2014</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    
                    <!--Styled Pagination-->
                    <div class="styled-pagination text-center">
                        <ul class="clearfix">
                            <li><a href="#"><span class="fa fa-caret-left"></span></a></li>
                            <li><a href="#" class="active">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#"><span class="fa fa-caret-right"></span></a></li>
                        </ul>
                    </div>
                    <!--End Styled Pagination-->
                    
                </div>
                
                <!--Form Column-->
                <div class="form-column col-lg-3 col-md-4 col-sm-12 col-xs-12">
                	
                    <!--Select Car Tabs-->
                    <div class="select-cars-tabs">
                        <!--Tabs Box-->
                        <div class="prod-tabs tabs-box">
                        
                            <!--Tab Btns-->
                            <ul class="tab-btns tab-buttons clearfix">
                                <li data-tab="#prod-new-cars" class="tab-btn active-btn">New Cars</li>
                                <li data-tab="#prod-used-cars" class="tab-btn">Used Cars</li>
                            </ul>
                            
                            <!--Tabs Container-->
                            <div class="tabs-content">
                                
                                <!--Tab / Active Tab-->
                                <div class="tab active-tab" id="prod-new-cars">
                                    <div class="content">
                                        
                                        <!--Cars Form-->
                                        <div class="cars-form">
                                            <form method="post" action="contact.html">
                                                
                                                <div class="form-group">
                                                    <label>Make:</label>
                                                    <select class="custom-select-box">
                                                        <option>Any Make</option>
                                                        <option>Make One</option>
                                                        <option>Make Two</option>
                                                        <option>Make Three</option>
                                                        <option>Make Four</option>
                                                    </select>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label>Model:</label>
                                                    <select class="custom-select-box">
                                                        <option>Any Model</option>
                                                        <option>Model Two</option>
                                                        <option>Model Three</option>
                                                        <option>Model Four</option>
                                                        <option>Model Five</option>
                                                    </select>
                                                </div>
												<div class="row clearfix">
                                                	<div class="form-group inner-group col-md-6 col-sm-6 col-xs-12">
                                                        <label>Min Price:</label>
                                                        <select class="custom-select-box">
                                                            <option>$300000</option>
                                                            <option>$400000</option>
                                                            <option>$500000</option>
                                                            <option>$600000</option>
                                                            <option>$700000</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group inner-group col-md-6 col-sm-6 col-xs-12">
                                                        <label>Max Price:</label>
                                                        <select class="custom-select-box">
                                                            <option>$400000</option>
                                                            <option>$500000</option>
                                                            <option>$600000</option>
                                                            <option>$700000</option>
                                                            <option>$800000</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label>Colors:</label>
                                                    <select class="custom-select-box">
                                                        <option>Titanium Metalic</option>
                                                        <option>Color 1</option>
                                                        <option>Color 2</option>
                                                        <option>Color 3</option>
                                                        <option>Color 4</option>
                                                    </select>
                                                </div>
                                                
                                                <div class="form-group">
                                                	<button class="theme-btn btn-style-one">Find a Car</button>
                                                </div>
                                                
                                                
                                            </form>
                                        </div>
                                        
                                    </div>
                                </div>
                                
                                <!--Tab-->
                                <div class="tab" id="prod-used-cars">
                                    <div class="content">
                                        
                                       	<!--Cars Form-->
                                        <div class="cars-form">
                                            <form method="post" action="contact.html">
                                                
                                                <div class="form-group">
                                                    <label>Make:</label>
                                                    <select class="custom-select-box">
                                                        <option>Any Make</option>
                                                        <option>Make One</option>
                                                        <option>Make Two</option>
                                                        <option>Make Three</option>
                                                        <option>Make Four</option>
                                                    </select>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label>Model:</label>
                                                    <select class="custom-select-box">
                                                        <option>Any Model</option>
                                                        <option>Model Two</option>
                                                        <option>Model Three</option>
                                                        <option>Model Four</option>
                                                        <option>Model Five</option>
                                                    </select>
                                                </div>
                                                
                                                   
                                                <div class="row clearfix">
                                                	<div class="form-group inner-group col-md-6 col-sm-6 col-xs-12">
                                                        <label>Min Price:</label>
                                                        <select class="custom-select-box">
                                                            <option>$300000</option>
                                                            <option>$400000</option>
                                                            <option>$500000</option>
                                                            <option>$600000</option>
                                                            <option>$700000</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group inner-group col-md-6 col-sm-6 col-xs-12">
                                                        <label>Max Price:</label>
                                                        <select class="custom-select-box">
                                                            <option>$400000</option>
                                                            <option>$500000</option>
                                                            <option>$600000</option>
                                                            <option>$700000</option>
                                                            <option>$800000</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="form-group">
                                                    <label>Colors:</label>
                                                    <select class="custom-select-box">
                                                        <option>Titanium Metalic</option>
                                                        <option>Color 1</option>
                                                        <option>Color 2</option>
                                                        <option>Color 3</option>
                                                        <option>Color 4</option>
                                                    </select>
                                                </div>
                                                
                                                <div class="form-group">
                                                	<button class="theme-btn btn-style-one">Find a Car</button>
                                                </div>
                                                
                                               
                                            </form>
                                        </div>
                                        
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        
                    </div>
                    <!--End Select Car Tabs-->
                    
                </div>
            
			
			
			</div>
        </div>
    </section>
    <!--End Inventory Section-->
    
    <?php include("include/footer.php");?>
    
</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="icon fa fa-angle-up"></span></div>

<script src="js/jquery.js"></script> 
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.fancybox.pack.js"></script>
<script src="js/jquery.fancybox-media.js"></script>
<script src="js/owl.js"></script>
<script src="js/appear.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/wow.js"></script>
<script src="js/script.js"></script>

</body>
</html>
