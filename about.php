<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>CarWorld HTML Template | About</title>
<!-- Stylesheets -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">

<!--Favicon-->
<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
<link rel="icon" href="images/favicon.png" type="image/x-icon">
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>

<body>
<div class="page-wrapper">
 	
    <?php include("include/header.php");?>
    
    <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/5.jpg);">
        <div class="auto-container">
            <h1>About Us</h1>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Page Info-->
    <section class="page-info">
        <div class="auto-container">
            <ul class="bread-crumb">
                <li><a href="index.html">Home</a></li>
                <li>Pages</li>
                <li class="current">About Us</li>
            </ul>
        </div>
    </section>
    <!--End Page Info-->
    
    <!--About Section-->
    <section class="about-section">
        <div class="auto-container">
            <!--Sec Title-->
            <div class="sec-title">
                <h2>About Carworld</h2>
            </div>
            <div class="row clearfix">
                <!--Content Column-->
                <div class="content-column col-md-4 col-sm-12 col-xs-12">
                    <div class="inner-column">
                        <div class="bold-text">We Have The Right Products to Fit Your Needs <span class="theme_color">Purchase - Carworld.</span></div>
                        <div class="text">Carworld brings 41 years of interior designs experience right to home or office. Our design professionals are equipped to help you determine the products and design that work best for our customers within the colors and lighting of your we make more than your expectation and your dream designs.</div>
                        <div class="clearfix">
                            <div class="pull-left">
                                <div class="signature">
                                    <img src="images/resource/signature.png" alt="" />
                                </div>
                            </div>
                            <div class="pull-right">
                                <h3>William Shocks,</h3>
                                <span class="designation">CEO & Founder</span>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Blocks Column-->
                <div class="blocks-column col-md-8 col-sm-12 col-xs-12">
                    <div class="row clearfix">
                        <!--About Block-->
                        <div class="about-block col-md-6 col-sm-6 col-xs-12">
                            <div class="inner-box">
                                <div class="image">
                                    <img src="images/resource/about-1.jpg" alt="" />
                                </div>
                                <div class="lower-box">
                                    <h3><a href="#">Our Mission</a></h3>
                                    <div class="text">To work in accordance with the clients’ requirement and exceed their expectations in terms of quality, cost control and time management.</div>
                                </div>
                            </div>
                        </div>
                        
                        <!--About Block-->
                        <div class="about-block col-md-6 col-sm-6 col-xs-12">
                            <div class="inner-box">
                                <div class="image">
                                    <img src="images/resource/about-2.jpg" alt="" />
                                </div>
                                <div class="lower-box">
                                    <h3><a href="#">Our Vision</a></h3>
                                    <div class="text">To consistently deliver eco-friendly world class finishes in our interior design concepts, execute & complete all projects in such a way.</div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End About Section-->
    
    <!--Advantage Section-->
    <section class="advantage-section">
    	<div class="auto-container">
        	<div class="row clearfix">
            	<!--Blocks Column-->
            	<div class="blocks-column col-lg-8 col-md-12 col-sm-12 col-xs-12">
                	<div class="sec-title light">
                    	<h2>Our Advantages</h2>
                    </div>
                    <div class="row clearfix">
                    	<!--Services Block-->
                        <div class="services-block-three col-md-6 col-sm-6 col-xs-12">
                        	<div class="inner-box">
                            	<div class="icon-box">
                                	<span class="icon flaticon-car-search"></span>
                                </div>
                                <h3><a href="services.html">25 Years of Experience</a></h3>
                                <div class="sub-title">With Quality Service</div>
                                <div class="text">How all this mistakens idea off denouncing pleasures and praisings ut pain.</div>
                            </div>
                        </div>
                        
                        <!--Services Block-->
                        <div class="services-block-three col-md-6 col-sm-6 col-xs-12">
                        	<div class="inner-box">
                            	<div class="icon-box">
                                	<span class="icon flaticon-steering-wheel"></span>
                                </div>
                                <h3><a href="services.html">Quality Products</a></h3>
                                <div class="sub-title">At Low Prices</div>
                                <div class="text">Denouncing pleasures and praisings pains was born work will gives you.</div>
                            </div>
                        </div>
                        
                        <!--Services Block-->
                        <div class="services-block-three col-md-6 col-sm-6 col-xs-12">
                        	<div class="inner-box">
                            	<div class="icon-box">
                                	<span class="icon flaticon-networking"></span>
                                </div>
                                <h3><a href="services.html">Exclusive Partnership</a></h3>
                                <div class="sub-title">Easy Finance</div>
                                <div class="text">Idea of denouncing pleasure and praisings pain born and system and expound.</div>
                            </div>
                        </div>
                        
                        <!--Services Block-->
                        <div class="services-block-three col-md-6 col-sm-6 col-xs-12">
                        	<div class="inner-box">
                            	<div class="icon-box">
                                	<span class="icon flaticon-support"></span>
                                </div>
                                <h3><a href="services.html">Customer Support</a></h3>
                                <div class="sub-title">24/7 Online Support</div>
                                <div class="text">Idea of denouncing pleasure and praisings pain born and system and expound.</div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <!--Customer Column-->
                <div class="customer-column col-lg-4 col-md-12 col-sm-12 col-xs-12">
                	<div class="inner-box">
                    	<div class="upper-box">
                        	<div class="icon flaticon-24-hours"></div>
                            <h3>24/7 Customer Care</h3>
                            <div class="title">For Emergency Service</div>
                        </div>
                        <div class="lower-box">
                        	<div class="number">1800-456-789</div>
                            <div class="text">We can provide expert 24 hour Emergency Service, Contact when you need it!.</div>
                            <h4>For queries:</h4>
                            <ul>
                            	<li><span class="theme_color">Tel:</span> (+321) 123-45-678</li>
                                <li><span class="theme_color">Email:</span> Mailus@Carworld.com </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End Advantage Section-->
    
    <!--Team Section-->
    <section class="team-section">
    	<div class="auto-container">
        	<!--Sec Title-->
            <div class="sec-title centered">
            	<h2>Meet Our team</h2>
            </div>
            <div class="two-item-carousel owl-carousel owl-theme">
            	
                <!--Team Block-->
            	<div class="team-block">
                	<div class="inner-box">
                    	<div class="clearfix">
                        	<div class="image-column col-md-6 col-sm-6 col-xs-12">
                            	<div class="image">
                                	<img src="images/resource/team-1.jpg" alt="" />
                                    <div class="overlay-box">
                                    	<ul class="social-icon-one">
                                        	<li><a href="#"><span class="fa fa-facebook"></span></a></li>
                                            <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                            <li><a href="#"><span class="fa fa-linkedin"></span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="content-column col-md-6 col-sm-6 col-xs-12">
                            	<div class="content-inner">
                                	<h3><a href="#">Michael Jeorge</a></h3>
                                    <div class="sub-title">CEO & Founder</div>
                                    <div class="text">Explain to you how this mistaken <br> idea of denouncing pleasure.</div>
                                    <ul class="list-style-three">
                                        <li><span class="icon fa fa-phone"></span>Ph: 900-789-0123</li>
                                        <li><span class="icon fa fa-envelope"></span>Email: Jeorge@Carworld.com</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Team Block-->
            	<div class="team-block">
                	<div class="inner-box">
                    	<div class="clearfix">
                        	<div class="image-column col-md-6 col-sm-6 col-xs-12">
                            	<div class="image">
                                	<img src="images/resource/team-2.jpg" alt="" />
                                    <div class="overlay-box">
                                    	<ul class="social-icon-one">
                                        	<li><a href="#"><span class="fa fa-facebook"></span></a></li>
                                            <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                            <li><a href="#"><span class="fa fa-linkedin"></span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="content-column col-md-6 col-sm-6 col-xs-12">
                            	<div class="content-inner">
                                	<h3><a href="#">Stephen Fernando</a></h3>
                                    <div class="sub-title">VP Sales & Marketing</div>
                                    <div class="text">Actual teachings of  great explorer of the truth, the master.</div>
                                    <ul class="list-style-three">
                                        <li><span class="icon fa fa-phone"></span>Ph: 900-123-4567</li>
                                        <li><span class="icon fa fa-envelope"></span>Email: Stephen@Carworld.com</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Team Block-->
            	<div class="team-block">
                	<div class="inner-box">
                    	<div class="clearfix">
                        	<div class="image-column col-md-6 col-sm-6 col-xs-12">
                            	<div class="image">
                                	<img src="images/resource/team-1.jpg" alt="" />
                                    <div class="overlay-box">
                                    	<ul class="social-icon-one">
                                        	<li><a href="#"><span class="fa fa-facebook"></span></a></li>
                                            <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                            <li><a href="#"><span class="fa fa-linkedin"></span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="content-column col-md-6 col-sm-6 col-xs-12">
                            	<div class="content-inner">
                                	<h3><a href="#">Michael Jeorge</a></h3>
                                    <div class="sub-title">CEO & Founder</div>
                                    <div class="text">Explain to you how this mistaken <br> idea of denouncing pleasure.</div>
                                    <ul class="list-style-three">
                                        <li><span class="icon fa fa-phone"></span>Ph: 900-789-0123</li>
                                        <li><span class="icon fa fa-envelope"></span>Email: Jeorge@Carworld.com</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Team Block-->
            	<div class="team-block">
                	<div class="inner-box">
                    	<div class="clearfix">
                        	<div class="image-column col-md-6 col-sm-6 col-xs-12">
                            	<div class="image">
                                	<img src="images/resource/team-2.jpg" alt="" />
                                    <div class="overlay-box">
                                    	<ul class="social-icon-one">
                                        	<li><a href="#"><span class="fa fa-facebook"></span></a></li>
                                            <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                            <li><a href="#"><span class="fa fa-linkedin"></span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="content-column col-md-6 col-sm-6 col-xs-12">
                            	<div class="content-inner">
                                	<h3><a href="#">Stephen Fernando</a></h3>
                                    <div class="sub-title">VP Sales & Marketing</div>
                                    <div class="text">Actual teachings of  great explorer of the truth, the master.</div>
                                    <ul class="list-style-three">
                                        <li><span class="icon fa fa-phone"></span>Ph: 900-123-4567</li>
                                        <li><span class="icon fa fa-envelope"></span>Email: Stephen@Carworld.com</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Team Block-->
            	<div class="team-block">
                	<div class="inner-box">
                    	<div class="clearfix">
                        	<div class="image-column col-md-6 col-sm-6 col-xs-12">
                            	<div class="image">
                                	<img src="images/resource/team-1.jpg" alt="" />
                                    <div class="overlay-box">
                                    	<ul class="social-icon-one">
                                        	<li><a href="#"><span class="fa fa-facebook"></span></a></li>
                                            <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                            <li><a href="#"><span class="fa fa-linkedin"></span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="content-column col-md-6 col-sm-6 col-xs-12">
                            	<div class="content-inner">
                                	<h3><a href="#">Michael Jeorge</a></h3>
                                    <div class="sub-title">CEO & Founder</div>
                                    <div class="text">Explain to you how this mistaken <br> idea of denouncing pleasure.</div>
                                    <ul class="list-style-three">
                                        <li><span class="icon fa fa-phone"></span>Ph: 900-789-0123</li>
                                        <li><span class="icon fa fa-envelope"></span>Email: Jeorge@Carworld.com</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Team Block-->
            	<div class="team-block">
                	<div class="inner-box">
                    	<div class="clearfix">
                        	<div class="image-column col-md-6 col-sm-6 col-xs-12">
                            	<div class="image">
                                	<img src="images/resource/team-2.jpg" alt="" />
                                    <div class="overlay-box">
                                    	<ul class="social-icon-one">
                                        	<li><a href="#"><span class="fa fa-facebook"></span></a></li>
                                            <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                            <li><a href="#"><span class="fa fa-linkedin"></span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="content-column col-md-6 col-sm-6 col-xs-12">
                            	<div class="content-inner">
                                	<h3><a href="#">Stephen Fernando</a></h3>
                                    <div class="sub-title">VP Sales & Marketing</div>
                                    <div class="text">Actual teachings of  great explorer of the truth, the master.</div>
                                    <ul class="list-style-three">
                                        <li><span class="icon fa fa-phone"></span>Ph: 900-123-4567</li>
                                        <li><span class="icon fa fa-envelope"></span>Email: Stephen@Carworld.com</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!--End Team Section-->
    
    <!--Client Section-->
    <section class="client-section style-two">
    	<div class="auto-container">
        	<div class="row clearfix">
            	<!--Title Column-->
            	<div class="title-column col-md-4 col-sm-12 col-xs-12">
                	<div class="sec-title no-border">
                    	<h2>Who Trust us</h2>
                    </div>
                    <div class="style-text">Here are some of the brands that have trusted us for car performance.</div>
                    <div class="text">Great explorer of the truth, the master-builder of human happiness one rejects, dislikes, or avoids sed pleasure because it is pleasure.</div>
                   
                </div>
                <!--Client Column-->
                <div class="client-column col-md-8 col-sm-12 col-xs-12">
                	<div class="clients-box">
                    	<div class="clearfix">
                        
                        	<!--Client Box-->
                        	<div class="client-box col-md-3 col-sm-6 col-xs-12">
                            	<div class="image">
                                	<a href="#"><img src="images/clients/9.png" alt="" /></a>
                                </div>
                            </div>
                            
                            <!--Client Box-->
                        	<div class="client-box col-md-3 col-sm-6 col-xs-12">
                            	<div class="image">
                                	<a href="#"><img src="images/clients/10.png" alt="" /></a>
                                </div>
                            </div>
                            
                            <!--Client Box-->
                        	<div class="client-box col-md-3 col-sm-6 col-xs-12">
                            	<div class="image">
                                	<a href="#"><img src="images/clients/11.png" alt="" /></a>
                                </div>
                            </div>
                            
                            <!--Client Box-->
                        	<div class="client-box col-md-3 col-sm-6 col-xs-12">
                            	<div class="image">
                                	<a href="#"><img src="images/clients/12.png" alt="" /></a>
                                </div>
                            </div>
                            
                            <!--Client Box-->
                        	<div class="client-box col-md-3 col-sm-6 col-xs-12">
                            	<div class="image">
                                	<a href="#"><img src="images/clients/13.png" alt="" /></a>
                                </div>
                            </div>
                            
                            <!--Client Box-->
                        	<div class="client-box col-md-3 col-sm-6 col-xs-12">
                            	<div class="image">
                                	<a href="#"><img src="images/clients/14.png" alt="" /></a>
                                </div>
                            </div>
                            
                            <!--Client Box-->
                        	<div class="client-box col-md-3 col-sm-6 col-xs-12">
                            	<div class="image">
                                	<a href="#"><img src="images/clients/15.png" alt="" /></a>
                                </div>
                            </div>
                            
                            <!--Client Box-->
                        	<div class="client-box col-md-3 col-sm-6 col-xs-12">
                            	<div class="image">
                                	<a href="#"><img src="images/clients/16.png" alt="" /></a>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End Client Section-->
    
    <?php include("include/footer.php");?>

</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="icon fa fa-angle-up"></span></div>

<script src="js/jquery.js"></script> 
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.fancybox.pack.js"></script>
<script src="js/jquery.fancybox-media.js"></script>
<script src="js/owl.js"></script>
<script src="js/appear.js"></script>
<script src="js/wow.js"></script>
<script src="js/script.js"></script>

</body>
</html>
