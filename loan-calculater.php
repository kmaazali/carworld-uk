<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>CarWorld</title>
<!-- Stylesheets -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">

<!--Favicon-->
<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
<link rel="icon" href="images/favicon.png" type="image/x-icon">
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>

<body>
<div class="page-wrapper">
 	
    
    <?php include("include/header.php");?>
    <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/5.jpg);">
        <div class="auto-container">
            <h1>Loan Calculator</h1>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Page Info-->
    <section class="page-info">
        <div class="auto-container">
            <ul class="bread-crumb">
                <li><a href="index.html">Home</a></li>
                <li>Pages</li>
                <li class="current">Auto Loan Calculator</li>
            </ul>
        </div>
    </section>
    <!--End Page Info-->
    
    <!--Loan Section-->
    <section class="loan-section">
    	<div class="auto-container">
        	<div class="row clearfix">
            	<!--Calculate Column-->
            	<div class="calculate-column col-lg-9 col-md-8 col-sm-12 col-xs-12">
                	<div class="inner-column">
                        <!--Sec Title-->
                        <div class="sec-title">
                            <h2>Calculate Your Emi</h2>
                        </div>
                        <div class="text">Use our Car Loan EMI (Easy/Equated Monthly Installment) calculator to calculate the amount that you will have to pay per month towards your loan repayment basic principal loan amount, the interest rate and the car loan term. </div>
                        
                        <!--Calculate Form-->
                        <div class="calculate-form">
                        	<div class="form-box">
                                <form method="post" action="calculater-form">
                                    <div class="row clearfix">
                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <label>Vehicle Price ($)</label>
                                            <input type="text" name="fname" value="" placeholder="Name" required>
                                        </div>
                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <label>Down Payment ($)</label>
                                            <select class="custom-select-box">
                                                <option>20000</option>
                                                <option>30000</option>
                                                <option>40000</option>
                                                <option>50000</option>
                                                <option>60000</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <label>Term (Month)</label>
                                            <select class="custom-select-box">
                                                <option>24</option>
                                                <option>25</option>
                                                <option>26</option>
                                                <option>27</option>
                                                <option>28</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <label>Interest Rate</label>
                                            <select class="custom-select-box">
                                                <option>8.9</option>
                                                <option>9.0</option>
                                                <option>9.1</option>
                                                <option>9.2</option>
                                                <option>9.3</option>
                                            </select>
                                        </div>
                                        
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <button type="submit" class="theme-btn btn-style-one">Calculate Now</button>
                                        </div>
                                        
                                    </div>
                                    
                                </form>
                            </div>
                            <div class="lower-box">
                            	<h3>Your Monthly Car Loan EMI: <span class="theme_color">$21906</span></h3>
                                <div class="amount">Know Total Interest and Total Amount of Your Car.</div>
                                <div class="plus-box"><a href="#" class="icon flaticon-plus"></a> <div class="text">hover me</div></div>
                            </div>
                            
                        </div>
                        <!--End Calculate Form-->
                        
                    </div>
                </div>
                <!--Faq Column-->
                <div class="form-column col-lg-3 col-md-4 col-sm-12 col-xs-12">
                	
                    <!-- Search Box -->
                    <div class="faq-search-box">
                    	<div class="outer-box">
                            <form method="post" action="contact.html">
                                <div class="form-group">
                                    <input type="search" name="search-field" value="" placeholder="Search" required>
                                    <button type="submit"><span class="icon fa fa-search"></span></button>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                    <!--Select Car Tabs-->
                    <div class="select-cars-tabs">
                        <!--Tabs Box-->
                        <div class="prod-tabs tabs-box">
                        
                            <!--Tab Btns-->
                            <ul class="tab-btns tab-buttons clearfix">
                                <li data-tab="#prod-new-cars" class="tab-btn active-btn">New Cars</li>
                                <li data-tab="#prod-used-cars" class="tab-btn">Used Cars</li>
                            </ul>
                            
                            <!--Tabs Container-->
                            <div class="tabs-content">
                                
                                <!--Tab / Active Tab-->
                                <div class="tab active-tab" id="prod-new-cars">
                                    <div class="content">
                                        
                                        <!--Cars Form-->
                                        <div class="cars-form">
                                            <form method="post" action="contact.html">
                                                
                                                <div class="form-group">
                                                    <label>Make:</label>
                                                    <select class="custom-select-box">
                                                        <option>Any Make</option>
                                                        <option>Make One</option>
                                                        <option>Make Two</option>
                                                        <option>Make Three</option>
                                                        <option>Make Four</option>
                                                    </select>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label>Model:</label>
                                                    <select class="custom-select-box">
                                                        <option>Any Model</option>
                                                        <option>Model Two</option>
                                                        <option>Model Three</option>
                                                        <option>Model Four</option>
                                                        <option>Model Five</option>
                                                    </select>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label>Body:</label>
                                                    <select class="custom-select-box">
                                                        <option>Convertible</option>
                                                        <option>Convertible 1</option>
                                                        <option>Convertible 2</option>
                                                        <option>Convertible 3</option>
                                                        <option>Convertible 4</option>
                                                    </select>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label>Year:</label>
                                                    <select class="custom-select-box">
                                                        <option>2012-2013</option>
                                                        <option>2013-2014</option>
                                                        <option>2014-2015</option>
                                                        <option>2015-2016</option>
                                                        <option>2016-2017</option>
                                                    </select>
                                                </div>
                                                        
                                                <div class="row clearfix">
                                                	<div class="form-group inner-group col-md-6 col-sm-6 col-xs-12">
                                                        <label>Min Price:</label>
                                                        <select class="custom-select-box">
                                                            <option>$300000</option>
                                                            <option>$400000</option>
                                                            <option>$500000</option>
                                                            <option>$600000</option>
                                                            <option>$700000</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group inner-group col-md-6 col-sm-6 col-xs-12">
                                                        <label>Max Price:</label>
                                                        <select class="custom-select-box">
                                                            <option>$400000</option>
                                                            <option>$500000</option>
                                                            <option>$600000</option>
                                                            <option>$700000</option>
                                                            <option>$800000</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label>Transmission:</label>
                                                    <select class="custom-select-box">
                                                        <option>Semi Automatic</option>
                                                        <option>Automatic 1</option>
                                                        <option>Automatic 2</option>
                                                        <option>Automatic 3</option>
                                                        <option>Automatic 4</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Colors:</label>
                                                    <select class="custom-select-box">
                                                        <option>Titanium Metalic</option>
                                                        <option>Color 1</option>
                                                        <option>Color 2</option>
                                                        <option>Color 3</option>
                                                        <option>Color 4</option>
                                                    </select>
                                                </div>
                                                
                                                <div class="form-group">
                                                	<button class="theme-btn btn-style-one">Find a Car</button>
                                                </div>
                                                
                                                <div class="form-group">
                                                	<button type="reset" class="reset-all"><span class="fa fa-refresh"></span>Reset all</button>
                                                </div>
                                                
                                            </form>
                                        </div>
                                        
                                    </div>
                                </div>
                                
                                <!--Tab-->
                                <div class="tab" id="prod-used-cars">
                                    <div class="content">
                                        
                                       	<!--Cars Form-->
                                        <div class="cars-form">
                                            <form method="post" action="contact.html">
                                                
                                                <div class="form-group">
                                                    <label>Make:</label>
                                                    <select class="custom-select-box">
                                                        <option>Any Make</option>
                                                        <option>Make One</option>
                                                        <option>Make Two</option>
                                                        <option>Make Three</option>
                                                        <option>Make Four</option>
                                                    </select>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label>Model:</label>
                                                    <select class="custom-select-box">
                                                        <option>Any Model</option>
                                                        <option>Model Two</option>
                                                        <option>Model Three</option>
                                                        <option>Model Four</option>
                                                        <option>Model Five</option>
                                                    </select>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label>Body:</label>
                                                    <select class="custom-select-box">
                                                        <option>Convertible</option>
                                                        <option>Convertible 1</option>
                                                        <option>Convertible 2</option>
                                                        <option>Convertible 3</option>
                                                        <option>Convertible 4</option>
                                                    </select>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label>Year:</label>
                                                    <select class="custom-select-box">
                                                        <option>2012-2013</option>
                                                        <option>2013-2014</option>
                                                        <option>2014-2015</option>
                                                        <option>2015-2016</option>
                                                        <option>2016-2017</option>
                                                    </select>
                                                </div>
                                                        
                                                <div class="row clearfix">
                                                	<div class="form-group inner-group col-md-6 col-sm-6 col-xs-12">
                                                        <label>Min Price:</label>
                                                        <select class="custom-select-box">
                                                            <option>$300000</option>
                                                            <option>$400000</option>
                                                            <option>$500000</option>
                                                            <option>$600000</option>
                                                            <option>$700000</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group inner-group col-md-6 col-sm-6 col-xs-12">
                                                        <label>Max Price:</label>
                                                        <select class="custom-select-box">
                                                            <option>$400000</option>
                                                            <option>$500000</option>
                                                            <option>$600000</option>
                                                            <option>$700000</option>
                                                            <option>$800000</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label>Transmission:</label>
                                                    <select class="custom-select-box">
                                                        <option>Semi Automatic</option>
                                                        <option>Automatic 1</option>
                                                        <option>Automatic 2</option>
                                                        <option>Automatic 3</option>
                                                        <option>Automatic 4</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Colors:</label>
                                                    <select class="custom-select-box">
                                                        <option>Titanium Metalic</option>
                                                        <option>Color 1</option>
                                                        <option>Color 2</option>
                                                        <option>Color 3</option>
                                                        <option>Color 4</option>
                                                    </select>
                                                </div>
                                                
                                                <div class="form-group">
                                                	<button class="theme-btn btn-style-one">Find a Car</button>
                                                </div>
                                                
                                                <div class="form-group">
                                                	<button type="reset" class="reset-all"><span class="fa fa-refresh"></span>Reset all</button>
                                                </div>
                                                
                                            </form>
                                        </div>
                                        
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        
                    </div>
                    <!--End Select Car Tabs-->
                    
                </div>
            </div>
        </div>
    </section>
    <!--End Loan Section-->
    
	<?php include("include/footer.php");?>
    
</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="icon fa fa-angle-up"></span></div>

<script src="js/jquery.js"></script> 
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.fancybox.pack.js"></script>
<script src="js/jquery.fancybox-media.js"></script>
<script src="js/owl.js"></script>
<script src="js/appear.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/wow.js"></script>
<script src="js/script.js"></script>

</body>
</html>
