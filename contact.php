<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>CarWorld</title>
<!-- Stylesheets -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">

<!--Favicon-->
<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
<link rel="icon" href="images/favicon.png" type="image/x-icon">
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>

<body>
<div class="page-wrapper">
 	
    <?php include("include/header.php")?>
    
    <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/5.jpg);">
        <div class="auto-container">
            <h1>Contact Us</h1>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Page Info-->
    <section class="page-info">
        <div class="auto-container">
            <ul class="bread-crumb">
                <li><a href="index.html">Home</a></li>
                <li>Pages</li>
                <li class="current">Contact Us</li>
            </ul>
        </div>
    </section>
    <!--End Page Info-->
    
    <!--Contact Form Section-->
    <section class="contact-form-section">
    	<div class="auto-container">
        	<div class="sec-title">
            	<h2>Send Message Us</h2>
                <div class="text">Don’t hestiate to ask us something, Our customer support team always ready to help you, <br> they will support you 24/7.</div>
            </div>
        	
            <!--Contact Form-->
            <div class="contact-form">
                <form method="post" action="sendemail.php" id="contact-form">
                    <div class="row clearfix">
                        <div class="column col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <input type="text" name="username" placeholder="Your Name *" required>
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" placeholder="Email Address *" required>
                            </div>
                            <div class="form-group">
                                <input type="text" name="phone" placeholder="Phone Num" required>
                            </div>
                        </div>
                        
                        <div class="column col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <textarea name="message" placeholder="Enter Your Message..."></textarea>
                            </div>
                        </div>
                        <div class="column btn-column col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <button class="theme-btn btn-style-one" type="submit" name="submit-form">Submit Now</button>
                            </div>
                        </div>
                    </div>
                    
                </form>
            </div>
        </div>
    </section>
    <!--End Contact Form Section-->
    
    <!--Contact Detailed Section-->
    <section class="contact-detailed-section">
    	<div class="auto-container">
        	<!--Sec Title-->
            <div class="sec-title light centered">
            	<h2>Contact Details</h2>
            </div>
            <div class="row clearfix">
            	<!--Headquater-->
                <div class="column col-md-4 col-sm-6 col-xs-12">
                	<div class="headquater-box">
                    	<div class="inner-box">
                        	<h2>Headquaters</h2>
                            <ul class="list-style-six">
                            	<li><span class="icon flaticon-maps-and-flags"></span><span class="bold">Address:</span>321 Car World, Breaking Str, 2nn st, Newyork ,USA 10002.</li>
                                <li><span class="icon flaticon-telephone"></span><span class="bold">Call Us:</span><br>+321 4567 89 012 & 79 023</li>
                                <li><span class="icon flaticon-e-mail-envelope"></span><span class="bold">Mail Us:</span><br>Supportteam@Carworld.com</li>
                            </ul>
                            <ul class="social-icon-four">
                            	<li><a href="#"><span class="fa fa-facebook"></span></a></li>
                                <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                                <li><a href="#"><span class="fa fa-linkedin"></span></a></li>
                                <li><a href="#"><span class="fa fa-skype"></span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--Sales-->
                <div class="column col-md-4 col-sm-6 col-xs-12">
                	<!--Sales Department-->
                	<div class="sales-department">
                    	<div class="inner-box">
                        	<h2>Sales Department</h2>
                            <div class="single-item-carousel owl-carousel owl-theme">
                            
                            	<div class="slide">
                                    <div class="department-author">
                                        <div class="inner-box">
                                            <div class="content">
                                                <div class="image">
                                                    <img src="images/resource/author-7.jpg" alt="" />
                                                </div>
                                                <h3>Charles Mecky</h3>
                                                <ul>
                                                    <li><span class="icon fa fa-phone"></span>84578-25-658</li>
                                                    <li><span class="icon fa fa-envelope"></span>Charlesmeck@gmail.com</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                            
                                    <div class="department-author">
                                        <div class="inner-box">
                                            <div class="content">
                                                <div class="image">
                                                    <img src="images/resource/author-8.jpg" alt="" />
                                                </div>
                                                <h3>Robert Fertly</h3>
                                                <ul>
                                                    <li><span class="icon fa fa-phone"></span>98765-43-210</li>
                                                    <li><span class="icon fa fa-envelope"></span>Robertfert@gmail.com</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="slide">
                                    <div class="department-author">
                                        <div class="inner-box">
                                            <div class="content">
                                                <div class="image">
                                                    <img src="images/resource/author-7.jpg" alt="" />
                                                </div>
                                                <h3>Charles Mecky</h3>
                                                <ul>
                                                    <li><span class="icon fa fa-phone"></span>84578-25-658</li>
                                                    <li><span class="icon fa fa-envelope"></span>Charlesmeck@gmail.com</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                            
                                    <div class="department-author">
                                        <div class="inner-box">
                                            <div class="content">
                                                <div class="image">
                                                    <img src="images/resource/author-8.jpg" alt="" />
                                                </div>
                                                <h3>Robert Fertly</h3>
                                                <ul>
                                                    <li><span class="icon fa fa-phone"></span>98765-43-210</li>
                                                    <li><span class="icon fa fa-envelope"></span>Robertfert@gmail.com</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="slide">
                                    <div class="department-author">
                                        <div class="inner-box">
                                            <div class="content">
                                                <div class="image">
                                                    <img src="images/resource/author-7.jpg" alt="" />
                                                </div>
                                                <h3>Charles Mecky</h3>
                                                <ul>
                                                    <li><span class="icon fa fa-phone"></span>84578-25-658</li>
                                                    <li><span class="icon fa fa-envelope"></span>Charlesmeck@gmail.com</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                            
                                    <div class="department-author">
                                        <div class="inner-box">
                                            <div class="content">
                                                <div class="image">
                                                    <img src="images/resource/author-8.jpg" alt="" />
                                                </div>
                                                <h3>Robert Fertly</h3>
                                                <ul>
                                                    <li><span class="icon fa fa-phone"></span>98765-43-210</li>
                                                    <li><span class="icon fa fa-envelope"></span>Robertfert@gmail.com</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <!--Hours-->
                <div class="column col-md-4 col-sm-12 col-xs-12">
                	<div class="hours-block">
                    	<div class="inner-box">
                        	<h2>Working Hours</h2>
                            <ul>
                            	<li class="clearfix">Monday<span>09:00am - 08:00pm</span></li>
                                <li class="clearfix">Tuesday<span>09:00am - 06:00pm</span></li>
                                <li class="clearfix">Wednesday<span>10:00am - 08:00pm</span></li>
                                <li class="clearfix">Thursday<span>09:00am - 08:00pm</span></li>
                                <li class="clearfix">Friday<span>09:00am - 06:00pm</span></li>
                                <li class="clearfix">Saturday<span>10:00am - 02:00pm</span></li>
                                <li class="clearfix">Sunday<span class="closed">Close</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End Contact Detailed Section-->
    
    <!--Map Section-->
    <section class="map-section">
    	<!--Map Outer-->
        <div class="map-outer">
            <div class="google-map"
                id="contact-google-map" 
                data-map-lat="44.231172" 
                data-map-lng="-76.485954" 
                data-icon-path="images/icons/map-marker.png" 
                data-map-title="Alabama, USA" 
                data-map-zoom="12" 
                data-markers='{
                    "marker-2": [44.231172, -76.485954, "<h4>Branch Office</h4><p>4/99 Alabama, USA</p>"],
                    "marker-4": [40.880550, -77.393705, "<h4>Branch Office</h4><p>4/99 Pennsylvania, USA</p>"]
                }'>

    		</div>
        </div>
    </section>
	<!--End Map Section-->
    
   
	<?php include("include/footer.php")?>
    
</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="icon fa fa-angle-up"></span></div>

<script src="js/jquery.js"></script> 
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.fancybox.pack.js"></script>
<script src="js/jquery.fancybox-media.js"></script>
<script src="js/owl.js"></script>
<script src="js/appear.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/wow.js"></script>
<script src="js/validate.js"></script>
<script src="js/script.js"></script>

<!--Google Map APi Key-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHzPSV2jshbjI8fqnC_C4L08ffnj5EN3A"></script>
<script src="js/gmaps.js"></script>
<script src="js/map-script.js"></script>
<!--End Google Map APi-->

</body>
</html>
