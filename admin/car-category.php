<?php include("include/config.php");
if(isset($_GET['del'])){
	$del_id = $_GET['del'];
	
	$del_query = "DELETE FROM `categories` WHERE `id`= $del_id";
	if(mysqli_query($conn,$del_query)){
		$success="Category Deleted";
	}
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Add Car Category</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <meta name="description" content="Elephant is an admin template that helps you build modern Admin Applications, professionally fast! Built on top of Bootstrap, it includes a large collection of HTML, CSS and JS components that are simple to use and easy to customize.">
    <meta property="og:url" content="http://demo.madebytilde.com/elephant">
    <meta property="og:type" content="website">
    <meta property="og:title" content="The fastest way to build Modern Admin APPS for any platform, browser, or device.">
    <meta property="og:description" content="Elephant is an admin template that helps you build modern Admin Applications, professionally fast! Built on top of Bootstrap, it includes a large collection of HTML, CSS and JS components that are simple to use and easy to customize.">
    <meta property="og:image" content="http://demo.madebytilde.com/elephant.jpg">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@madebytilde">
    <meta name="twitter:creator" content="@madebytilde">
    <meta name="twitter:title" content="The fastest way to build Modern Admin APPS for any platform, browser, or device.">
    <meta name="twitter:description" content="Elephant is an admin template that helps you build modern Admin Applications, professionally fast! Built on top of Bootstrap, it includes a large collection of HTML, CSS and JS components that are simple to use and easy to customize.">
    <meta name="twitter:image" content="http://demo.madebytilde.com/elephant.jpg">
    <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="manifest.json">
    <link rel="mask-icon" href="safari-pinned-tab.svg" color="#d9230f">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
    <link rel="stylesheet" href="css/vendor.min.css">
    <link rel="stylesheet" href="css/elephant.min.css">
    <link rel="stylesheet" href="css/application.min.css">
  </head>
  <body class="layout layout-header-fixed">
    <?php include("include/header.php");?>
    <div class="layout-main">
		<?php include("include/sidebar.php");?>
      
	  
	  
		<div class="layout-content">
			<div class="layout-content-body">
				<div class="text-center m-b">
					<h3 class="m-b-0">Car Brand</h3>
					
					<?php
									if(isset($_GET['error'])){
										echo"<span style='color:red;'>".$_GET['error']."</span>";
									}
									else if(isset($_GET['Success'])){
										echo"<span style='color:green;'>".$_GET['Success']."</span>";
									}
									?>
				</div>
				<div class="row">
					<div class="col-sm-6 col-sm-offset-3">
						<div class="demo-md-form-wrapper">
							
							<form action="include/process.php" method="post">
								
								<div class="md-form-group md-label-floating">
								  <input class="md-form-control" type="text" name="cat-name">
								  <label class="md-control-label">Add Category</label>
								  
								  
								</div>
								
								<input class="btn btn-default pull-right" type="submit" name="submit" Value="Add Category">
								
							</form>
						</div>
					  
					</div>
				</div>
				
				<div class="row gutter-xs">
					<div class="col-md-6">
					  <div class="card">
						<div class="card-header">
						  <div class="card-actions">
							<button type="button" class="card-action card-toggler" title="Collapse"></button>
							
						  </div>
						  <strong>ALL Cars Category</strong>
						</div>
						<?php 
							if(isset($success)){
								echo"<span style='color:red; margin-left:16px;'>$success</span>";
							}
						?>
						<div class="card-body" data-toggle="match-height" style="height: 263px;">
						  <table class="table table-hover table-bordered table-striped">
							<thead>
							  <tr>
								<th class="text-left">#</th>
								<th class="text-left">Category Name</th>
								
								<th class="text-center">Actions</th>
							  </tr>
							</thead>
							<tbody>
								<?php 
									$query="SELECT * FROM categories order by id desc";
									$run= mysqli_query($conn,$query);
									if(mysqli_num_rows($run)> 0){
										while($row = mysqli_fetch_array($run)){
											$id=$row['id'];
											$cat_name =$row['cat-name'];
									
								?>	
							  <tr>
								<td class="text-left"><?php echo $id;?></td>
								<td class="text-left"><a href="#"><?php echo $cat_name;?></a></td>
								
								
								<td class="text-center">
								  <div class="dropdown">
									<button class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
									  More
									  <span class="caret"></span>
									</button>
									<ul class="dropdown-menu dropdown-menu-right">
									  <li class="disabled"><a href="#">Edit</a></li>
									  <li><a href="car-category.php?del=<?php echo $id; ?>">Delete</a></li>
									  
									</ul>
								  </div>
								</td>
							  </tr>
								<?php
									}
									}
									else{
										echo"No Category Found";
									}
							 
								?>	
							</tbody>
						  </table>
						</div>
					  </div>
					</div>
					
				
				</div>
        
        
				
			</div>
		</div>
      <?php include("include/footer.php")?>
    </div>
    
	<script src="js/vendor.min.js"></script>
    <script src="js/elephant.min.js"></script>
    <script src="js/application.min.js"></script>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-83990101-1', 'auto');
      ga('send', 'pageview');
    </script>
  </body>
</html>