<div class="layout-sidebar">
        <div class="layout-sidebar-backdrop"></div>
        <div class="layout-sidebar-body">
          <div class="custom-scrollbar">
            <nav id="sidenav" class="sidenav-collapse collapse">
              <ul class="sidenav">
                <li class="sidenav-search hidden-md hidden-lg">
                  <form class="sidenav-form" action="/">
                    <div class="form-group form-group-sm">
                      <div class="input-with-icon">
                        <input class="form-control" type="text" placeholder="Search…">
                        <span class="icon icon-search input-icon"></span>
                      </div>
                    </div>
                  </form>
                </li>
                
                
                <li class="sidenav-heading">Components</li>
                <li class="sidenav-item has-subnav">
                  <a href="#" aria-haspopup="true">
                    <span class="sidenav-icon icon icon-briefcase"></span>
                    <span class="sidenav-label">Cars</span>
                  </a>
                  <ul class="sidenav-subnav collapse">
                    
					<li class="sidenav-subheading">Cars</li>
                    <li><a href="order-enquiry.php">Order Enquiry <span class="badge badge-success">1</span></a></li>
					<li><a href="car-view">All Cars</a></li>
                    <li><a href="car-category.php">Add Category</a></li>
                    <li><a href="car-add.php">Add New Car</a></li>
                    <li><a href="car-edit">Edit Car</a></li>
                    <li><a href="car-profile">Car Details</a></li>
                    
                    
                  </ul>
                </li>
                
			  
			  </ul>
            </nav>
          </div>
        </div>
      </div>