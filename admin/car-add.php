<?php
	include("include/config.php");
	
	
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <meta name="description" content="Elephant is an admin template that helps you build modern Admin Applications, professionally fast! Built on top of Bootstrap, it includes a large collection of HTML, CSS and JS components that are simple to use and easy to customize.">
    <meta property="og:url" content="http://demo.madebytilde.com/elephant">
    <meta property="og:type" content="website">
    <meta property="og:title" content="The fastest way to build Modern Admin APPS for any platform, browser, or device.">
    <meta property="og:description" content="Elephant is an admin template that helps you build modern Admin Applications, professionally fast! Built on top of Bootstrap, it includes a large collection of HTML, CSS and JS components that are simple to use and easy to customize.">
    <meta property="og:image" content="http://demo.madebytilde.com/elephant.jpg">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@madebytilde">
    <meta name="twitter:creator" content="@madebytilde">
    <meta name="twitter:title" content="The fastest way to build Modern Admin APPS for any platform, browser, or device.">
    <meta name="twitter:description" content="Elephant is an admin template that helps you build modern Admin Applications, professionally fast! Built on top of Bootstrap, it includes a large collection of HTML, CSS and JS components that are simple to use and easy to customize.">
    <meta name="twitter:image" content="http://demo.madebytilde.com/elephant.jpg">
    <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="manifest.json">
    <link rel="mask-icon" href="safari-pinned-tab.svg" color="#d9230f">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
    <link rel="stylesheet" href="css/vendor.min.css">
    <link rel="stylesheet" href="css/elephant.min.css">
    <link rel="stylesheet" href="css/application.min.css">
  </head>
  <body class="layout layout-header-fixed">
    <?php include("include/header.php");?>
	<div class="layout-main">
      <?php include("include/sidebar.php");?>
		<div class="layout-content">
			<div class="layout-content-body">
				
					<div class="title-bar">
						<h1 class="title-bar-title">Add New Car</h1>
					</div>
					<div class="row">
						<div class="col-md-10" style="">
							<form method="post" action="include/process.php" enctype="multipart/form-data">
								<div class="row">
									<div class="col-xs-9">
										<label>Title</label>
											<input class="form-control" type="text" placeholder="Title" name="name">
									 </div>
								</div>
								<div class="row">
									<div class="col-xs-3">
										<label>Registration Number</label>
											<input class="form-control" type="text" placeholder="Registration Number" name="reg">
									 </div>
									 <div class="col-xs-3">
										<label>Mile Age</label>
											<input class="form-control" type="number" placeholder="Mile Age" name="mile">
									 </div>
									 <div class="col-xs-3">
										<label>Make</label>
											<input class="form-control" type="text" placeholder="Make" name="make">
									 </div>
								</div>
								<div class="row">
									
									 <div class="col-xs-3">
										<label>Model</label>
											<input class="form-control" type="number" placeholder="Model" name="model">
									 </div>
									 <div class="col-xs-3">
										<label>Price</label>
											<input class="form-control" type="number" placeholder="Price" name="price">
									 </div>
									 <div class="col-xs-3">
										<label>Color</label>
											<input class="form-control" type="text" placeholder="Color" name="color">
									 </div>
								</div>
								<div class="row">
									
								</div>
								<div class="row">
									<div class="col-xs-3">
										<label>Fuel Type</label>
											<select id="form-control-6" class="form-control" name="fuel">
											
											<option value="select">Select</option>
											<option value="BI-Fuel">Bi Fuell</option>
											<option value="Diesel">Diesel</option>
											<option value="Electric">Electric</option>
											<option value="Electric">Petrol</option>
											
										  </select>
									 </div>
									 <div class="col-xs-3">
										<label>Gear Box</label>
											<select id="form-control-6" class="form-control" name="gear">
											
											<option value="select">Select</option>
											<option value="Auto">Auto</option>
											<option value="Manual">Manual</option>
											<option value="Hybrid">Hybrid</option>
											
										  </select>
									 </div>
									 <div class="col-xs-3">
										<label>Doors</label>
											<select id="form-control-6" class="form-control" name="door">
											
											<option value="select">Select</option>
											<option value="Four">4</option>
											<option value="Five">5</option>
											
										  </select>
									 </div>
								</div>
								<div class="row">
									
									 <div class="col-xs-3">
										<label>Seats</label>
											<select id="form-control-6" class="form-control" name="seats">
											
											<option value="select">Select</option>
											<option value="Four">4</option>
											<option value="Five">5</option>
											
										  </select>
									 </div>
									 <div class="col-xs-3">
										<label>Condition</label>
										<select id="form-control-6" class="form-control" name="condition">
											
											<option value="select">Select</option>
											<option value="New">New</option>
											<option value="Used">Used</option>
											
										  </select>
									</div>
								</div>
								<br>
								<p class="form-control-static"><b style="color:green;">Performence</b></p>
								<div class="row">
									<div class="col-xs-3">
										<label>Acceleration</label>
											<input class="form-control" type="number" placeholder="Acceleration" name="acc">
									 </div>
									 <div class="col-xs-3">
										<label>Top Speed</label>
											<input class="form-control" type="number" placeholder="Speed" name="speed">
									 </div>
								</div>
								<br>
								<p class="form-control-static"><b style="color:green;">Safety</b></p>
								<div class="row">
									<div class="col-xs-3">
										<label>Air Bag Driver</label>
											<select id="form-control-6" class="form-control" name="abdriver">
											<option value="select">Select</option>
											<option value="Yes">Yes</option>
											<option value="No">No</option>
											
										  </select>
									 </div>
									 <div class="col-xs-3">
										<label>Air Bag Passenger</label>
											<select id="form-control-6" class="form-control" name="abpass">
											
											<option value="select">Select</option>
											<option value="Yes">Yes</option>
											<option value="No">No</option>
											
										  </select>
									 </div>
								</div>

								<div class="row">
									
									
										<div class="col-xs-3" style="margin-top:10px;">
											<label>Upload Image</label>
											<input id="form-control-9" type="file" accept="image/*" multiple="multiple" name="image">
										</div>
									 
								</div>

								<div class="row">
									<div class="col-xs-9">
											
											<input type="submit" class="btn btn-default pull-right" name="addcar" value="Submit">
									 </div>
									 
								</div>

							</form>
						</div>
					</div>
				
			</div>
		</div>
	  <div class="layout-footer">
        <div class="layout-footer-body">
          <small class="version">Version 1.0.0</small>
          <small class="copyright">2018 &copy; Hostechpk <a href="#"></a></small>
        </div>
      </div>
    </div>
    
	<script src="js/vendor.min.js"></script>
    <script src="js/elephant.min.js"></script>
    <script src="js/application.min.js"></script>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-83990101-1', 'auto');
      ga('send', 'pageview');
    </script>
  </body>
</html>