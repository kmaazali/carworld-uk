<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>All Cars</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <meta name="description" content="Elephant is an admin template that helps you build modern Admin Applications, professionally fast! Built on top of Bootstrap, it includes a large collection of HTML, CSS and JS components that are simple to use and easy to customize.">
    <meta property="og:url" content="http://demo.madebytilde.com/elephant">
    <meta property="og:type" content="website">
    <meta property="og:title" content="The fastest way to build Modern Admin APPS for any platform, browser, or device.">
    <meta property="og:description" content="Elephant is an admin template that helps you build modern Admin Applications, professionally fast! Built on top of Bootstrap, it includes a large collection of HTML, CSS and JS components that are simple to use and easy to customize.">
    <meta property="og:image" content="http://demo.madebytilde.com/elephant.jpg">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@madebytilde">
    <meta name="twitter:creator" content="@madebytilde">
    <meta name="twitter:title" content="The fastest way to build Modern Admin APPS for any platform, browser, or device.">
    <meta name="twitter:description" content="Elephant is an admin template that helps you build modern Admin Applications, professionally fast! Built on top of Bootstrap, it includes a large collection of HTML, CSS and JS components that are simple to use and easy to customize.">
    <meta name="twitter:image" content="http://demo.madebytilde.com/elephant.jpg">
    <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="manifest.json">
    <link rel="mask-icon" href="safari-pinned-tab.svg" color="#d9230f">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
    <link rel="stylesheet" href="css/vendor.min.css">
    <link rel="stylesheet" href="css/elephant.min.css">
    <link rel="stylesheet" href="css/application.min.css">
    <link rel="stylesheet" href="css/demo.min.css">
  </head>
  <body class="layout layout-header-fixed">
    <?php include("include/header.php");?>
	<div class="layout-main">
      <?php include("include/sidebar.php");?>
	  <div class="layout-content">
        <div class="layout-content-body">
          <div class="title-bar">
            <h1 class="title-bar-title">
              <span class="d-ib">DataTables
                <small>Responsive Extension</small>
              </span>
              <span class="d-ib">
                <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                  <span class="sr-only">Add to shortcut list</span>
                </a>
              </span>
            </h1>
            <p class="title-bar-description">
              <small>The tables presented below use <a href="https://datatables.net/extensions/responsive/" target="_blank">DataTables Responsive Extension</a>, the styling of which is completely rewritten in SASS, without modifying however anything in JavaScript.</small>
            </p>
          </div>
          <div class="row gutter-xs">
            <div class="col-xs-12">
              <div class="card">
                <div class="card-header">
                  <div class="card-actions">
                    <button type="button" class="card-action card-toggler" title="Collapse"></button>
                    <button type="button" class="card-action card-reload" title="Reload"></button>
                    <button type="button" class="card-action card-remove" title="Remove"></button>
                  </div>
                  <strong>Responsive Table</strong>
                </div>
                <div class="card-body">
                  <div id="demo-datatables-responsive-1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
				  <div class="row">
				  
				  
				  </div>
				  <div class="row">
				  <div class="col-sm-12">
				  <table id="demo-datatables-responsive-1" class="table table-striped table-nowrap dataTable no-footer dtr-inline" cellspacing="0" width="100%" aria-describedby="demo-datatables-responsive-1_info" role="grid" style="width: 100%;">
                    <thead>
                      <tr role="row"><th class="sorting" tabindex="0" aria-controls="demo-datatables-responsive-1" rowspan="1" colspan="1" style="width: 122px;" aria-label="First name: activate to sort column ascending">First name</th><th class="sorting" tabindex="0" aria-controls="demo-datatables-responsive-1" rowspan="1" colspan="1" style="width: 120px;" aria-label="Last name: activate to sort column ascending">Last name</th><th class="sorting" tabindex="0" aria-controls="demo-datatables-responsive-1" rowspan="1" colspan="1" style="width: 224px;" aria-label="Position: activate to sort column ascending">Position</th><th class="sorting" tabindex="0" aria-controls="demo-datatables-responsive-1" rowspan="1" colspan="1" style="width: 114px;" aria-label="Office: activate to sort column ascending">Office</th><th class="sorting" tabindex="0" aria-controls="demo-datatables-responsive-1" rowspan="1" colspan="1" style="width: 70px;" aria-label="Age: activate to sort column ascending">Age</th><th class="sorting" tabindex="0" aria-controls="demo-datatables-responsive-1" rowspan="1" colspan="1" style="width: 116px;" aria-label="Start date: activate to sort column ascending">Start date</th><th class="sorting_desc" tabindex="0" aria-controls="demo-datatables-responsive-1" rowspan="1" colspan="1" style="width: 91px;" aria-sort="descending" aria-label="Salary: activate to sort column ascending">Salary</th><th class="sorting" tabindex="0" aria-controls="demo-datatables-responsive-1" rowspan="1" colspan="1" style="width: 79px;" aria-label="Extn.: activate to sort column ascending">Extn.</th><th class="sorting" tabindex="0" aria-controls="demo-datatables-responsive-1" rowspan="1" colspan="1" style="width: 221px;" aria-label="E-mail: activate to sort column ascending">E-mail</th></tr>
                    </thead>
                    <tbody>
                      
                    <tr role="row" class="odd">
                        <td tabindex="0">Angelica</td>
                        <td>Ramos</td>
                        <td>Chief Executive Officer (CEO)</td>
                        <td>London</td>
                        <td>47</td>
                        <td>2009/10/09</td>
                        <td class="sorting_1">$1,200,000</td>
                        <td>5797</td>
                        <td>a.ramos@datatables.net</td>
                      </tr><tr role="row" class="even">
                        <td tabindex="0">Fiona</td>
                        <td>Green</td>
                        <td>Chief Operating Officer (COO)</td>
                        <td>San Francisco</td>
                        <td>48</td>
                        <td>2010/03/11</td>
                        <td class="sorting_1">$850,000</td>
                        <td>2947</td>
                        <td>f.green@datatables.net</td>
                      </tr><tr role="row" class="odd">
                        <td tabindex="0">Paul</td>
                        <td>Byrd</td>
                        <td>Chief Financial Officer (CFO)</td>
                        <td>New York</td>
                        <td>64</td>
                        <td>2010/06/09</td>
                        <td class="sorting_1">$725,000</td>
                        <td>3059</td>
                        <td>p.byrd@datatables.net</td>
                      </tr><tr role="row" class="even">
                        <td tabindex="0">Yuri</td>
                        <td>Berry</td>
                        <td>Chief Marketing Officer (CMO)</td>
                        <td>New York</td>
                        <td>40</td>
                        <td>2009/06/25</td>
                        <td class="sorting_1">$675,000</td>
                        <td>6154</td>
                        <td>y.berry@datatables.net</td>
                      </tr><tr role="row" class="odd">
                        <td tabindex="0">Jackson</td>
                        <td>Bradshaw</td>
                        <td>Director</td>
                        <td>New York</td>
                        <td>65</td>
                        <td>2008/09/26</td>
                        <td class="sorting_1">$645,750</td>
                        <td>1042</td>
                        <td>j.bradshaw@datatables.net</td>
                      </tr><tr role="row" class="even">
                        <td tabindex="0">Charde</td>
                        <td>Marshall</td>
                        <td>Regional Director</td>
                        <td>San Francisco</td>
                        <td>36</td>
                        <td>2008/10/16</td>
                        <td class="sorting_1">$470,600</td>
                        <td>6741</td>
                        <td>c.marshall@datatables.net</td>
                      </tr><tr role="row" class="odd">
                        <td tabindex="0">Vivian</td>
                        <td>Harrell</td>
                        <td>Financial Controller</td>
                        <td>San Francisco</td>
                        <td>62</td>
                        <td>2009/02/14</td>
                        <td class="sorting_1">$452,500</td>
                        <td>9422</td>
                        <td>v.harrell@datatables.net</td>
                      </tr><tr role="row" class="even">
                        <td tabindex="0">Cedric</td>
                        <td>Kelly</td>
                        <td>Senior Javascript Developer</td>
                        <td>Edinburgh</td>
                        <td>22</td>
                        <td>2012/03/29</td>
                        <td class="sorting_1">$433,060</td>
                        <td>6224</td>
                        <td>c.kelly@datatables.net</td>
                      </tr><tr role="row" class="odd">
                        <td tabindex="0">Tatyana</td>
                        <td>Fitzpatrick</td>
                        <td>Regional Director</td>
                        <td>London</td>
                        <td>19</td>
                        <td>2010/03/17</td>
                        <td class="sorting_1">$385,750</td>
                        <td>1965</td>
                        <td>t.fitzpatrick@datatables.net</td>
                      </tr><tr role="row" class="even">
                        <td tabindex="0">Brielle</td>
                        <td>Williamson</td>
                        <td>Integration Specialist</td>
                        <td>New York</td>
                        <td>61</td>
                        <td>2012/12/02</td>
                        <td class="sorting_1">$372,000</td>
                        <td>4804</td>
                        <td>b.williamson@datatables.net</td>
                      </tr></tbody>
                  </table></div></div>
				  <div class="row">
				  <div class="col-sm-6"></div>
				  <div class="col-sm-6">
					<div class="dataTables_paginate paging_simple_numbers" id="demo-datatables-responsive-1_paginate">
						<ul class="pagination">
								<li class="paginate_button previous disabled" id="demo-datatables-responsive-1_previous"><a href="#" aria-controls="demo-datatables-responsive-1" data-dt-idx="0" tabindex="0">«</a></li>
								<li class="paginate_button active"><a href="#" aria-controls="demo-datatables-responsive-1" data-dt-idx="1" tabindex="0">1</a></li>
								<li class="paginate_button "><a href="#" aria-controls="demo-datatables-responsive-1" data-dt-idx="2" tabindex="0">2</a></li>
								<li class="paginate_button "><a href="#" aria-controls="demo-datatables-responsive-1" data-dt-idx="3" tabindex="0">3</a></li>
								<li class="paginate_button "><a href="#" aria-controls="demo-datatables-responsive-1" data-dt-idx="4" tabindex="0">4</a></li>
								<li class="paginate_button "><a href="#" aria-controls="demo-datatables-responsive-1" data-dt-idx="5" tabindex="0">5</a></li>
								<li class="paginate_button "><a href="#" aria-controls="demo-datatables-responsive-1" data-dt-idx="6" tabindex="0">6</a></li>
								<li class="paginate_button next" id="demo-datatables-responsive-1_next"><a href="#" aria-controls="demo-datatables-responsive-1" data-dt-idx="7" tabindex="0">»</a></li>
						</ul>
					</div>
				</div>
				</div>
				</div>
                </div>
              </div>
            </div>
          </div>
		  
		  
		</div>
      </div>
      <?php include("include/footer.php")?>
    </div>
    <script src="js/vendor.min.js"></script>
    <script src="js/elephant.min.js"></script>
    <script src="js/application.min.js"></script>
    <script src="js/demo.min.js"></script>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-83990101-1', 'auto');
      ga('send', 'pageview');
    </script>
  </body>
</html>